<html>
<?php 
    include 'base/head.php';
?>
    <body class="patient_section tabs_page not_to_take">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid fullwidth">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                    <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>
                </div>
            </div>
            <div class="patient_content hcp_container h_mid">
                <div class="fl fullwidth res_about_us">
                    <div class="patient_container h_mid nav_container">
                        <div class="fullwidth fl main_nav res_nav">
                            <?php include 'includes/patient/nav2.php';?>
                        </div>
                        <img src="images/patient/tab3Res.png" alt="Talk To Your Doctor" class="h_mid"/>
                    </div>
                </div>
            </div>
            <!--Starting Tab number 3's Detail content section "By default its display none"-->

            <div class="fl fullwidth tabs-section">
                <div class="fl fullwidth patient_container h_mid">
                    <ul class="fl fullwidth">
                        <li class="main_det_li">
                            <img src="images/patient/tab3_1.png" class="fl"/>
                            <div class="tab_row_des fr">
                                <p class="fl desHe">
                                    <span class="fl desHpink">50,000 IU/ml oral solution</span>
                                    <span class="fl desHgrey">and</span>
                                    <span class="fl desHblue">InVita D3 25,000</span>
                                </p>
                                <ul class="det_per_row fl fullwidth">
                                    <li>
                                        <p>Are allergic to vitamin D or any of the other ingredients (tocopherol acetate, polyglyceryl oleate (E475), olive oil, refined, sweet orange peel oil)</p>
                                    </li>
                                    <li>
                                        <p> Have high levels of calcium in the blood (hypercalcaemia)</p>
                                    </li>
                                    <li>
                                        <p> Have high levels of calcium in the urine (hypercalciuria)</p>
                                    </li>
                                    <li>
                                        <p>Have disturbed parathyroid hormone metabolism (pseudohypoparathyroidism)</p>
                                    </li>
                                    <li>
                                        <p> Have kidney stones (renal calculi)</p>
                                    </li>
                                    <li>
                                        <p>  Have high levels of vitamin D in the blood (hypervitaminosis D)</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="main_det_li">
                            <img src="images/patient/tab3_2.png" class="fl"/>
                            <div class="tab_row_des fr">
                                <p class="fl desHe desHgreen">
                                    InVita D3 2,400 IU/ml oral drops, solution
                                </p>
                                <ul class="det_per_row fl fullwidth">
                                    <li>
                                        <p> Are allergic to vitamin D or any of the other ingredients of this medicine. (Listed in  section 6 of the package leaflet)</p>
                                    </li>
                                    <li>
                                        <p>Have high levels of calcium in your blood (hypercalcaemia) or urine (hypercalciuria)</p>
                                    </li>
                                    <li>
                                        <p>  Have kidney stones (renal calculi)</p>
                                    </li>
                                    <li>
                                        <p> Have high levels of vitamin D in your blood (hypervitaminosis D)</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="main_det_li">
                            <img src="images/patient/tab3_3.png" class="fl"/>
                            <div class="tab_row_des fr">
                                <p class="fl desHe desHpinkL">
                                    InVita D3 800 IU soft gel capsules
                                </p>
                                <ul class="det_per_row fl fullwidth">
                                    <li>
                                        <p> If you are under 12 years old</p>
                                    </li>
                                    <li>
                                        <p> Are allergic to vitamin D or any of the other ingredients of this medicine. (Listed in section 6 of the package leaflet)</p>
                                    </li>
                                    <li>
                                        <p> Have high levels of calcium in your blood (hypercalcaemia) or urine (hypercalciuria)</p>
                                    </li>
                                    <li>
                                        <p> Have kidney stones (renal calculi)</p>
                                    </li>
                                    <li>
                                        <p> Have high levels of vitamin D in your blood (hypervitaminosis D)</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!--Ending Tab number 3's Detail content section "By default its display none"-->
            
            <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>