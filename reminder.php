<!DOCTYPE html>
<html>
       <?php 
    include 'base/head.php';
?>
    <body class="patient_section treatment_page">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid fullwidth">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                        <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>
                </div>
            </div>
            <div class="patient_content hcp_container h_mid">
                <div class="fl bg_banner fullwidth">
                    <div class="patient_container h_mid nav_container">
                        <div class="fullwidth fl main_nav res_nav">
                            <?php include 'includes/patient/nav2.php';?>
                        </div>
                    </div>
                    <img src="images/patient/reminder_bg.png" class="h_mid treatment_banner"/>
                </div>
                <div class="patient_container fullwidth h_mid treatment_det reminder_main wow fadeInDown">
                    <div class="fl fullwidth">
                        <h4 class="fl heading_blue">InVita D3 25,000 IU / 50,000 IU oral solution web calendar</h4>
                        <p class="fl desP marT_20 desHgrey">
                            We all lead busy lives and sometimes remembering 
                            exactly when to take your medicine can be a challenge. 
                            Our useful web calendar can be downloaded either as a pdf document 
                            or directly into your chosen calendar application (e.g. outlook) to remind you of exactly when to take your medicine.
                        </p>
                        <div class="fl fullwidth reminder_sec">
                            <div class="fl fullwidth reminder_row marB_20">
                                <h5 class="fl heading_blue">What dose have you been advised to take?</h5>
                                <select>
                                    <option>Select</option>
                                    <option>25,000 UI</option>
                                    <option>50,000 UI</option>
                                </select>
                            </div>
                            <div class="fl fullwidth reminder_row marB_20">
                                <h5 class="fl heading_blue">How often have you been told to take this dose?</h5>
                                <select>
                                    <option>Every</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option><option>4</option><option>5</option><option>6</option>
                                    <option>7</option><option>8</option><option>9</option><option>10</option>
                                    <option>11</option><option>12</option><option>13</option><option>14</option>
                                    <option>15</option><option>16</option><option>17</option><option>18</option>
                                    <option>19</option>
                                </select>
                                <select>
                                    <option>Days</option>
                                    <option>weeks</option>
                                    <option>Months</option>
                                </select>
                            </div>
                            <div class="fl fullwidth reminder_row marB_20">
                                <h5 class="fl heading_blue">For how long?</h5>
                                <select>
                                    <option>For</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option><option>4</option><option>5</option><option>6</option>
                                    <option>7</option><option>8</option><option>9</option><option>10</option>
                                    <option>11</option><option>12</option>
                                </select>
                                <select>
                                    <option>weeks</option>
                                    <option>Months</option>
                                </select>
                            </div>
                            <div class="fl fullwidth reminder_row marB_20">
                                <h5 class="fl heading_blue">When will you take your first dose?</h5>
                                <input type="number" placeholder="25/3/2017"/>
                            </div>
                            <div class="fl fullwidth reminder_row">
                                <a href="javascript:void(0);" class="btn btn_standered fl marT_20">Generate PDF Calendar</a>
                            </div>
                            <div class="fl fullwidth reminder_row marT_50">
                                <h4 class="fl heading_blue">Top tips for remembering your medicine:</h4>
                                <ol class="fl fullwidth reminder_list">
                                    <li class="fl desP marT_20 desHgrey">
                                        <b>1</b>Keep medicine in a place where it’s handy at the right time
                                        e.g. in your handbag or at work, next to your breakfast cereal, with your packed lunch, by your bed
                                    </li>
                                    <li class="fl desP marT_20 desHgrey">
                                        <b>2</b>Set yourself reminders
                                        e.g. write it on your calendar, set a phone alarm, ask a family member to remind you, write it on your to-do list
                                    </li>
                                    <li class="fl desP marT_20 desHgrey">
                                        <b>3</b>Monitor yourself
                                        Keep a record of when you’ve taken your medicine – and give yourself a treat if you don’t forget it
                                    </li>
<li class="fl desP marT_20 desHgrey">
                                        <b>4</b>Make it a habit
                                        Try and take your medicine at the same time and place every dose so you get into a habit
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>