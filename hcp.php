<!DOCTYPE html>
<html>
    <?php 
    include 'base/head.php';
?>
    <body class="hcp_section">
        <div class="wrapper h_mid fullwidth">
            <div class="hcp_container h_mid">
                <div class="fl fullwidth">
                   <?php include 'includes/hcp/top_search.php';?>
                </div>
                <div class="fl fullwidth">
                    <div class="container h_mid">
                        <div class="fr mobile_navigation">
                            <a href="javascript:void(0);" class="mobile_icon fr"></a>
                        </div>
                        <div class="navigation fr">
                            <?php include 'includes/hcp/nav1.php';?>
                        </div>
                        <?php include 'includes/hcp/logo.php';?>

                    </div>
                    <div class="fullwidth fl main_nav">
                        <div class="container h_mid res_nav">
                            <?php include 'includes/hcp/nav2.php';?>
                            <?php include 'includes/hcp/main_slider.php';?>
                        </div>
                    </div>
                </div>   
            </div>
            <div class="patient_container h_mid hcp_content">
                <div class="two_col_tp fullwidth fl wow fadeInDown">
                    <div class="tp_cta fl">
                        <h1>Treatment</h1>
                        <p>
                            NOS guidelines recommended a treatment loading dose of 300,000 IU for treatment of vitamin D deficiency. Invita D3 50,000 IU is 
                            given as a weekly dose over 6 weeks for the treatment of vitamin D deficiency and is suitable for adults. D3 invita 25,000 is suitable
                            for treatment in children and is taken once every 2 weeks for 6 weeks.
                        </p>
                        <a href="javascript:void(0);" class="fr">Read More</a>
                    </div>
                    <div class="tp_cta fl tp_grey">
                        <h1>Prevention</h1>
                        <p>
                            Prevention of vitamin D3 is recommended in groups who may be at risk from vitamin D deficiency. This include:
                        </p>
                        <ul>
                            <li>Older Patients (Particularly in the case of falls or fractures) or those who may be institutionalised or hospitalized</li>
                            <li>Dark skin individuals</li>
                        </ul>
                        <a href="javascript:void(0);" class="fr">Read More</a>
                    </div>
                </div>
                <div class="four_col_tp fullwidth fl wow fadeInDown">
                    <div class="tp_cta_sm tp_sm1 fl">
                        <img src="images/hcp/tp_cta_sm1.png"/>
                        <h1>NEW 50,000 IU/ml oral solution</h1>
                        <p>
                            The simplicity of a UK guideline-aligned 
                            treatment course delivered in just 
                            6 weekly doses1,3
                        </p>
                    </div>
                    <div class="tp_cta_sm tp_sm2 fl">
                        <img src="images/hcp/tp_cta_sm2.png"/>
                        <h1>25,000 IU/ml Oral solution</h1>
                        <p>
                            A licensed vitamin D3 preparation
                            suitable for patients of all ages 2
                        </p>
                    </div>
                    <div class="tp_cta_sm tp_sm3 fl">
                        <img src="images/hcp/tp_cta_sm3.png"/>
                        <h1>2,400 Oral drops</h1>
                        <p>
                            A licensed solution for use in 
                            pregnancy, breastfeeding and 
                            paediatrics aged 0-18 years 1
                        </p>
                    </div>
                    <div class="tp_cta_sm tp_sm4 fl">
                        <img src="images/hcp/tp_cta_sm4.png"/>
                        <h1>800 IU soft gel capsules </h1>
                        <p>
                            are licensed for the prophylaxis and 
                            treatment of vitamin D deficiency in 
                            adults and children over 12 years old.
                        </p>
                    </div>
                </div>
                <div class="fullwidth fl weather_cont wow fadeInDown">
                    <h1 class="fullwidth fl cntr_txt">Current weather where you are</h1>
                    <div class="one_col_tp h_mid">
                        <div class="inner_one_col h_mid">
                            <img src="images/hcp/sun.png" class="fl"/>
                            <div class="weather_det fr">
                                <h1 class="fl fullwidth cntr_txt temp_txt">21 <span>&#8451;</span></h1>
                                <span class="weather_border fl fullwidth"></span>
                                <p class="fl fullwidth cntr_txt">3:06 pm BST</p>
                                <p class="fl fullwidth cntr_txt">Mostly Sunny</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Footer-->
            <?php include 'includes/hcp/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/hcp/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/hcp/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>