<!DOCTYPE html>
<html>
    <?php 
    include 'base/head.php';
?>
    <body class="patient_section health_risks res__health_risks">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid fullwidth">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                        <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>
                </div>
            </div>
            <div class="patient_content hcp_container h_mid">
                <div class="fl bg_banner fullwidth">
                    <div class="patient_container h_mid nav_container">
                        <div class="fullwidth fl main_nav res_nav">
                            <?php include 'includes/patient/nav2.php';?>
                        </div>
                    </div>
                </div>
                <div class="patient_container tabs_pos fullwidth h_mid">
                    <h2 class="fl fullwidth main_heading">
                        About InVita D3 - Health Risks
                    </h2>
                    <div class="fl fullwidth man_figure wow fadeInDown">
                        <p class="fr desHgrey sm_p">Click on the hotpsots to reveal possible risks</p>
                        <!--                        fig_cross-->
                        <figure class="fig_animate fig_brain">
                            <a href="javascript:void(0);" class="fig_plus fr"></a>
                            <img src="images/patient/fig_brain.png" class="fr fig_det fig_brain_img"/>
                        </figure>

                        <figure class="fig_animate fig_resparitry">
                            <a href="javascript:void(0);" class="fig_plus fl"></a>
                            <img src="images/patient/fig_respiratary.png" class="fl fig_det"/>
                        </figure>

                        <figure class="fig_animate fig_circularity">
                            <a href="javascript:void(0);" class="fig_plus fr"></a>
                            <img src="images/patient/fig_circularity.png" class="fr fig_det"/>
                        </figure>

                        <figure class="fig_animate fig_pancreas">
                            <a href="javascript:void(0);" class="fig_plus fl"></a>
                            <img src="images/patient/fig_pancreas.png" class="fl fig_det"/>
                        </figure>

                        <figure class="fig_animate fig_bone">
                            <a href="javascript:void(0);" class="fig_plus fl"></a>
                            <img src="images/patient/fig_bone.png" class="fl fig_det"/>
                        </figure>
                        
                        <figure class="fig_animate fig_muscle">
                            <a href="javascript:void(0);" class="fig_plus fr"></a>
                            <img src="images/patient/fig_muscle.png" class="fr fig_det"/>
                        </figure>
                        
                        <figure class="fig_animate fig_other">
                            <a href="javascript:void(0);" class="fig_plus h_mid"></a>
                            <img src="images/patient/fig_other.png" class="h_mid fig_det"/>
                        </figure>
                    </div>
                </div>
            </div>
             <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>