<!DOCTYPE html>
<html>
        <?php 
    include 'base/head.php';
?>
    <body class="patient_section treatment_page">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid fullwidth">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                        <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>
                </div>
            </div>
            <div class="patient_content hcp_container h_mid">
                <div class="fl bg_banner fullwidth">
                    <div class="patient_container h_mid nav_container">
                        <div class="fullwidth fl main_nav res_nav">
                            <?php include 'includes/patient/nav2.php';?>
                        </div>
                    </div>
                    <img src="images/patient/treatment.jpg" class="h_mid treatment_banner"/>
                </div>
                <div class="patient_container seventywidth h_mid treatment_det wow fadeInDown">
                    <p class="desHgrey desP fl fullwidth">
                        If you are identified as either having vitamin D deficiency or as being at particular risk of developing vitamin D 
                        deficiency, your doctor may decide to prescribe a vitamin D medicine.
                    </p>
                    <p class="desHgrey desP fl fullwidth">
                        These are available as capsules, oral solutions and injections. The medicines can be taken either daily, 
                        weekly or monthly depending on the medicine your doctor chooses to treat you.
                    </p>
                    <p class="desHgrey desP fl fullwidth bold">
                        Vitamin D supplements
                    </p>
                    <p class="desHgrey desP fl fullwidth">
                        There are Vitamin D supplements that you can buy at your pharmacy. These are manufactured to different standards 
                        than prescription-only medicines such as InvitaD3. Food supplements containing vitamin D show a large variation 
                        in the amount of vitamin D contained in them – some have much less than the label says; others have much more. 
                        You can’t be sure how much you’re getting.
                    </p>
                    <p class="desHgrey desP fl fullwidth">
                        A prescription-only medicine, on the other hand, lets you know exactly how much you’re getting. You can be sure 
                        with a prescription-only medicine, such as InvitaD3, that your medicine will provide you with a consistent amount 
                        of vitamin D. 
                    </p>
                </div>
            </div>
             <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>