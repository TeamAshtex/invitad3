<!DOCTYPE html>
<html>
    <?php 
    include 'base/head.php';
?>
    <body class="patient_section treatment_page boosting_page">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid fullwidth">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                        <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>
                </div>
            </div>
            <div class="patient_content hcp_container h_mid">
                <div class="fl bg_banner fullwidth">
                    <div class="patient_container h_mid nav_container">
                        <div class="fullwidth fl main_nav res_nav">
                            <?php include 'includes/patient/nav2.php';?>
                        </div>
                    </div>
                    <img src="images/patient/boosting.jpg" class="h_mid boosting_banner"/>
                </div>
                <div class="patient_container fullwidth h_mid main_boost">
                    <div class="fl fullwidth">
                        <div class="fl boost_box boost_box1 actived_boost" data-tab="tab1">
                            <div class="fl inner_boost fullwidth">
                                <figure class="fl boost_bg boost_bg1"></figure>
                                <div class="fl fullwidth boost_heading">
                                    <h4 class="fl fullwidth cntr_txt">
                                        Increase sun exposure
                                    </h4>
                                </div>
                            </div>
<div class="h_mid inner_boostdet boost_tab1 res_boost">
                                <p class="fl fullwidth desP desHgrey">
                                    Most people get the amount of vitamin D they need from sunlight (ultraviolet B exposure [UVB]) on the skin. We dont 
                                    need much, just 20 30 minutes of sunlight on the face and forearms around the middle of the day 2 to 3 times a week, 
                                    in the summer months, is usually enough for people with fair skin, however those with darker skin, or the elderly, may 
                                    need longer. Unfortunately for half the year (from October to April), 90% of the UK lies too far north to receive enough 
                                    sunlight to allow our bodies to generate adequate supplies of vitamin D in the skin and so we may need to look to 
                                    other sources, such as our diets, if we are to obtain adequate levels of vitamin D.
                                </p>
                                <p class="fl fullwidth des11 desHgrey">
                                    <b>Important note: </b>exposure to the sun is not the same as sunbathing; the bare skin simply needs to be exposed to sunlight (directly, i.e. not through a window)
                                </p>
                            </div>
                            <figure class="boost_icon"></figure>
                        </div>

                        <div class="fl boost_box boost_box2" data-tab="tab2">
                            <div class="fl inner_boost fullwidth">
                                <figure class="fl boost_bg boost_bg2"></figure>
                                <div class="fl fullwidth boost_heading">
                                    <h4 class="fl fullwidth cntr_txt">
                                        With the food you eat
                                    </h4>
                                </div>
                            </div>
<div class="h_mid inner_boostdet boost_tab2 res_boost">
                                <p class="fl fullwidth desP desHgrey">
                                    Some foods that are easy to include in our everyday diet can help boost our vitamin D levels. These include:
                                </p>
                                <div class="fl fullwidth c_sep"></div>
                                <div class="fl fullwidth">
                                    <div class="fl boost_bullet1">
                                        <p class="fl fullwidth desP desHgrey">Oily fish such as salmon, sardines and mackeral(canned or fresh)</p>
                                    </div>
                                    <div class="fl boost_bullet1">
                                        <p class="fl fullwidth desP desHgrey">Egg yolk</p>
                                        <p class="fl fullwidth desP desHgrey">Fortified fat spreads</p>
                                    </div>
                                    <div class="fl boost_bullet1">
                                        <p class="fl fullwidth desP desHgrey">Fortified breakfast cereals </p>
                                        <p class="fl fullwidth desP desHgrey">Powdered milk</p>
                                    </div>
                                </div>
                                <div class="fl fullwidth c_sep"></div>
                                <p class="fl fullwidth desP desHgrey">
                                    People who follow a strict vegetarian or vegan diet, or a non-fish-eating diet may be at a greater risk of vitamin D deficiency.
                                </p>
                                <p class="fl fullwidth desP desHgrey">
                                    <b class="fl fullwidth">Did you know?</b>
                                    Extra lean ham is up there in the list of most vitamin D rich foods  3oz of extra lean ham contains almost 
                                    one eighth of the vitamin D we require on a daily basis.
                                </p>
                            </div>
                            <figure class="boost_icon"></figure>
                        </div>

                        <div class="fl boost_box boostbox_last boost_box3" data-tab="tab3">
                            <div class="fl inner_boost fullwidth">
                                <figure class="fl boost_bg boost_bg3"></figure>
                                <div class="fl fullwidth boost_heading">
                                    <h4 class="fl fullwidth cntr_txt">
                                        Increase sun exposure
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fl fullwidth boost_tabs">
                        <div class="h_mid inner_boostdet boost_tab1">
                            <p class="fl fullwidth desP desHgrey">
                                Most people get the amount of vitamin D they need from sunlight (ultraviolet B exposure [UVB]) on the skin. We don’t 
                                need much, just 20–30 minutes of sunlight on the face and forearms around the middle of the day 2–3 times a week, 
                                in the summer months, is usually enough for people with fair skin, however those with darker skin, or the elderly, may 
                                need longer. Unfortunately for half the year (from October to April), 90% of the UK lies too far north to receive enough 
                                sunlight to allow our bodies to generate adequate supplies of vitamin D in the skin and so we may need to look to 
                                other sources, such as our diets, if we are to obtain adequate levels of vitamin D.
                            </p>
                            <p class="fl fullwidth des11 desHgrey">
                                <b>Important note: </b>exposure to the sun is not the same as sunbathing; the bare skin simply needs to be exposed to sunlight (directly, i.e. not through a window)
                            </p>
                        </div>
                        <div class="h_mid inner_boostdet boost_tab2">
                            <p class="fl fullwidth desP desHgrey">
                                Some foods that are easy to include in our everyday diet can help boost our vitamin D levels. These include:
                            </p>
                            <div class="fl fullwidth c_sep"></div>
                            <div class="fl fullwidth">
                                <div class="fl boost_bullet1">
                                    <p class="fl fullwidth desP desHgrey">Oily fish such as salmon, sardines and mackeral(canned or fresh)</p>
                                </div>
                                <div class="fl boost_bullet1">
                                    <p class="fl fullwidth desP desHgrey">Egg yolk</p>
                                    <p class="fl fullwidth desP desHgrey">Fortified fat spreads</p>
                                </div>
                                <div class="fl boost_bullet1">
                                    <p class="fl fullwidth desP desHgrey">Fortified breakfast cereals </p>
                                    <p class="fl fullwidth desP desHgrey">Powdered milk</p>
                                </div>
                            </div>
                            <div class="fl fullwidth c_sep"></div>
                            <p class="fl fullwidth desP desHgrey">
                                People who follow a strict vegetarian or vegan diet, or a non-fish-eating diet may be at a greater risk of vitamin D deficiency.
                            </p>
                            <p class="fl fullwidth desP desHgrey">
                                <b class="fl fullwidth">Did you know?</b>
                                Extra lean ham is up there in the list of most vitamin D rich foods – 3oz of extra lean ham contains almost 
                                one eighth of the vitamin D we require on a daily basis.
                            </p>
                        </div>
                        <div class="h_mid inner_boostdet boost_tab3">
                            <p class="fl fullwidth desP desHgrey">
                                Most people get the amount of vitamin D they need from sunlight (ultraviolet B exposure [UVB]) on the skin. We don’t 
                                need much, just 20–30 minutes of sunlight on the face and forearms around the middle of the day 2–3 times a week, 
                                in the summer months, is usually enough for people with fair skin, however those with darker skin, or the elderly, may 
                                need longer. Unfortunately for half the year (from October to April), 90% of the UK lies too far north to receive enough 
                                sunlight to allow our bodies to generate adequate supplies of vitamin D in the skin and so we may need to look to 
                                other sources, such as our diets, if we are to obtain adequate levels of vitamin D.
                            </p>
                            <p class="fl fullwidth des11 desHgrey">
                                <b>Important note: </b>exposure to the sun is not the same as sunbathing; the bare skin simply needs to be exposed to sunlight (directly, i.e. not through a window)
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>