<ul class="fl my_menu">
                            <li class="fl for_subnav">
                                <a href="javascript:void(0);" class="fl">
                                    <p class="fl">About Vitamin D deficiency</p>
                                    <span class="menu_icon fl"></span>
                                </a>
<ul class="subnav">
<li>
<a href="whatisit.php">What is it?</a>
</li>

<li>
<a href="whoisatrisk.php">Who is at risk?</a>
</li>

<li>
<a href="symptoms.php">Symptoms and 
Diagnosis</a>
</li>

<li>
<a href="boosting.php">How can I boost my vitamin D levels?</a>
</li>

<li>
<a href="treatments.php">Treatments</a>
</li>


<li>
<a href="causes.php">Causes of deficiency</a>
</li>

<li>
<a href="healthrisk.php">Health risks</a>
</li>

</ul>
                            </li>
                            <li class="fl">
                                <a href="prevention.php" class="fl">
                                    <p class="fl">Prevention and treatment</p>
                                    <span class="menu_icon fl"></span>
                                </a>
                            </li>
                            <li class="fl">
                                <a href="about.php" class="fl">
                                    <p class="fl">About Invita D3</p>
                                    <span class="menu_icon fl"></span>
                                </a>
                            </li>
                            <li class="fl">
                                <a href="faq.php" class="fl">
                                    <p class="fl">FAQ</p>
                                    <span class="menu_icon fl"></span>
                                </a>
                            </li>
                            <li class="fl">
                                <a href="reminder.php" class="fl">
                                    <p class="fl">Medicine Reminders</p>
                                    <span class="menu_icon fl"></span>
                                </a>
                            </li>
                        </ul>
						<script>
						$(function () {
							var current_location = window.location.href;
							current_location = current_location.split("/");
							current_location = current_location[current_location.length - 1];
							$(".my_menu").find("a").each(function(){
									if($(this).attr("href").indexOf(current_location) >= 0){
										$(this).addClass("active");
									} 
							});
						});
						</script>

