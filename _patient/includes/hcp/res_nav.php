<div class="resposnsive_menu">
<a href="javascript:void(0);" class="close_nav_icon"></a>
<ul class="h_mid">
<li class="fl">
                                <a href="javascript:void(0);" class="fl">
                                    <p class="fl">Professional</p>
                                    <span class="menu_icon fl"></span>
                                </a>
                            </li>
<li class="fl">
                                <a href="javascript:void(0);" class="fl">
                                    <p class="fl">Patient</p>
                                    <span class="menu_icon fl"></span>
                                </a>
                            </li>
<li class="fl">
                                <a href="javascript:void(0);" class="fl">
                                    <p class="fl">Contact</p>
                                    <span class="menu_icon fl"></span>
                                </a>
                            </li>
<figure class="c_sep c_sepNav"></figure>
                            <li class="fl for_subnav for_res_subnav">
                                <a href="javascript:void(0);" class="fl">
                                    <p class="fl">About Vitamin D deficiency</p>
                                    <span class="menu_icon fl"></span>
                                </a>
<ul class="subnav res_subnav" style="display:none;">
<li>
<a href="javascript:void(0);">What is it?</a>
</li>

<li>
<a href="whoisatrisk.php">Who is at risk?</a>
</li>

<li>
<a href="javascript:void(0);">Symptoms and 
Diagnosis</a>
</li>

<li>
<a href="javascript:void(0);">How can I boost my vitamin D levels?</a>
</li>

<li>
<a href="treatments.php">Treatments</a>
</li>


<li>
<a href="causes.php">Causes of deficiency</a>
</li>

<li>
<a href="healthrisk.php">Health risks</a>
</li>

</ul>
                            </li>
                            <li class="fl">
                                <a href="javascript:void(0);" class="fl">
                                    <p class="fl">Prevention and treatment</p>
                                    <span class="menu_icon fl"></span>
                                </a>
                            </li>
                            <li class="fl">
                                <a href="about.php" class="fl">
                                    <p class="fl">About Invita D3</p>
                                    <span class="menu_icon fl"></span>
                                </a>
                            </li>
                            <li class="fl">
                                <a href="about.php" class="fl">
                                    <p class="fl">FAQ</p>
                                    <span class="menu_icon fl"></span>
                                </a>
                            </li>
                            <li class="fl">
                                <a href="javascript:void(0);" class="fl">
                                    <p class="fl">Medicine Reminders</p>
                                    <span class="menu_icon fl"></span>
                                </a>
                            </li>
</ul>
</div>