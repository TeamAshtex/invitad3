<!DOCTYPE html>
<html>
<?php 
    include 'base/head.php';
?>
    <body class="patient_section syptoms_page">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid fullwidth">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                        <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>
                </div>
            </div>
            <div class="patient_content hcp_container h_mid">
                <div class="fl bg_banner fullwidth">
                    <div class="patient_container h_mid nav_container">
                        <div class="fullwidth fl main_nav res_nav">
                            <?php include 'includes/patient/nav2.php';?>
                        </div>
                    </div>
                </div>
                <div class="patient_container tabs_pos fullwidth h_mid">
                    <h2 class="fl fullwidth main_heading">
                        About Vitamin D - Symptoms and diagnosis
                    </h2>
                </div>
            </div>
            <div class="patient_container h_mid fullwidth wow fadeInDown">
                <h4 class="fl fullwidth main_heading">
                    About Vitamin D - Symptoms and diagnosis
                </h4>    
                <p class="fl fullwidth desP desHgrey marT_10">
                    Vitamin D deficiency can be confirmed with a simple blood test - or your doctor may decide to treat or offer some lifestyle advice based 
                    on your symptoms and whether you are in at ‘at-risk’ group.
                </p>
                <div class="fl fullwidth marT_50">
                    <div class="fl symp_box">
                        <h4 class="fl fullwidth mar_0 heading_blue">Symptoms of vitamin D deficiency can include:</h4>
                        <ul class="fl fullwidth marT_10">
                            <li>
                                <p class="fl symp__det desP desHgrey">Muscle weakness and painful bones. </p>
                            </li>
                            <li>
                                <p class="fl symp__det desP desHgrey">A general sense of being unwell</p>
                            </li>
                            <li>
                                <p class="fl symp__det desP desHgrey">Feeling tired</p>
                            </li>
                            <li>
                                <p class="fl symp__det desP desHgrey">Vague aches and pains </p>
                            </li>
                        </ul>
                    </div>
                    <div class="fl symp_box">
                        <h4 class="fl fullwidth mar_0 heading_blue">Children with vitamin D deficiency can experience</h4>
                        <ul class="fl fullwidth marT_10">
                            <li>
                                <p class="fl symp__det desP desHgrey">Poor growth</p>
                            </li>
                            <li>
                                <p class="fl symp__det desP desHgrey">Tooth delay</p>
                            </li>
                            <li>
                                <p class="fl symp__det desP desHgrey">A condition known as Rickets. (a condition where bones can start to bow)</p>
                            </li>
                        </ul>
                    </div>
                    <div class="fl symp_box">
                        <h4 class="fl fullwidth mar_0 heading_blue">Vitamin D deficiency is not unusual</h4>
                        <ul class="fl fullwidth marT_10">
                            <li>
                                <p class="fl desP desHgrey">
                                    Research shows that just over 1 in 
                                    10 people in Europe have vitamin D 
                                    levels that are considered to be too 
                                    low for good bone health. These figures 
                                    are much higher in winter time.
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="fl fullwidth marT_50 symptoms_bottom wow fadeInDown">
                    <p class="desP cntr_txt h_mid">
                        If you or your child are experiencing some of these symptoms or fall into one of the ‘at-risk’ 
                        groups described, it is advisable to see a doctor or nurse and discuss your symptoms.
                    </p>
                </div>
            </div>
            <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>