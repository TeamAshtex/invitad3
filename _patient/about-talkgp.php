<html>
<?php 
    include 'base/head.php';
?>
    <body class="patient_section tabs_page dosage_tab">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid fullwidth">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                    <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>
                </div>
            </div>
            <div class="patient_content hcp_container h_mid">
                <div class="fl fullwidth res_about_us">
                    <div class="patient_container h_mid nav_container">
                        <div class="fullwidth fl main_nav res_nav">
                            <?php include 'includes/patient/nav2.php';?>
                        </div>
                        <img src="images/patient/tab4Res.png" alt="Talk To Your Doctor" class="h_mid"/>
                    </div>
                </div>
            </div>
            <!--Starting Tab number 4's Detail content section "By default its display none"-->
            <div class="fl fullwidth tabs-section">
                <div class="fl fullwidth patient_container h_mid">
                    <ul class="fl fullwidth tab4_lines">
                        <li>
                            <p>Are undergoing treatment with certain medicines used to treat heart disorders (e.g. cardiac glycosides, such as digoxin).</p>
                        </li>
                        <li>
                            <p>Have sarcoidosis (an immune system disorder which may cause increased levels of vitamin D in the body).</p>
                        </li>
                        <li>
                            <p>Are taking medicines containing vitamin D, or eating foods or milk enriched with vitamin D.</p>
                        </li>
                        <li>
                            <p>Are likely to be exposed to a lot of sunshine whilst using your medicine.</p>
                        </li>
                        <li>
                            <p>Take additional supplements containing calcium so that your doctor can monitor you to ensure your blood calcium levels remain within an acceptable range.</p>
                        </li>
                        <li>
                            <p>Have kidney damage or disease – as your doctor may want to measure the levels of calcium in your blood or urine.</p>
                        </li>
                    </ul>
                    <div class="fl fullwidth download_btn">
                        <a href="leaflet.php" class="fl btn_leaflet">Download Package Leaflet</a>
                    </div>
                </div>
                <div class="fl fullwidth tab1_bg">
                    <div class="fl fullwidth patient_container h_mid tab4_2">
                        <h2 class="fl fullwidth main_heading heading_blue">
                            Tell your doctor, pharmacist or nurse if you are using, have recently used or might use any other medicines:
                        </h2>
                        <ul class="fl fullwidth tab4_lines">
                            <li class="heading_li">
                                <p class="">This is specially important if you are taking:</p>
                            </li>
                            <li>
                                <p>Medicines that act on the heart or kidneys, such as cardiac glycosides (e.g. digoxin) or diuretics (e.g. bendroflumethazide).</p>
                            </li>
                            <li>
                                <p>Medicines containing vitamin D or food that are particularly rich in vitamin D such as some types of vitamin D-enriched milk.</p>
                            </li>
                            <li>
                                <p>Actinomycin (a medicine used to treat some forms of cancer) 
                                    and imidazole antifungals (e.g. clotrimazole and ketoconazole, 
                                    medicines used to treat fungal infections).</p>
                            </li>
                        </ul>
                        <ul class="fl fullwidth tab4_lines">
                            <li class="heading_li">
                                <p class="">This is specially important if you are taking:</p>
                            </li>
                            <li>
                                <p>Antiepileptic medicines (anticonvulsants), barbiturates.</p>
                            </li>
                            <li>
                                <p>Glucocorticoids (steroid hormones such as hydrocortisone or prednisolone).</p>
                            </li>
                            <li>
                                <p>Medicines that lower the level of cholesterol in the blood (such as cholestyramine, or colestipol).</p>
                            </li>
                            <li>
                                <p>Certain medicines for weight loss that reduce the amount of fat your body absorbs (e.g. orlistat).</p>
                            </li>
                            <li>
                                <p>Certain laxatives (such as liquid paraffin).</p>
                            </li>
                        </ul>
                        <div class="fl fullwidth download_btn">
                            <a href="leaflet.php" class="fr btn_leaflet">Download Package Leaflet</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--Ending Tab number 4's Detail content section "By default its display none"-->
            
            <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>