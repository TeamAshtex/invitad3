<!DOCTYPE html>
<html>
    <?php 
    include 'base/head.php';
?>
    <body class="patient_section">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                        <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>

                    <div class="fullwidth fl main_nav res_nav">
                        <?php include 'includes/patient/nav2.php';?>
                    </div>
                </div>
                <div class="patient_content fullwidth fl">
                    <div class="fl ar_sec">
                        <h2 class="fl fullwidth main_heading">
                            About InVita D3 - Health Risks
                        </h2>
                        <div class="fl at_risk wow fadeInDown">
                            <img src="images/patient/atrisk1.png" class="fl fullwidth"/>
                            <h4 class="fl fullwidth desHwhite cntr_txt risk_lable">Infants and children under 5 years of age</h4>
                        </div>
                        <div class="fr at_risk wow fadeInDown">
                            <img src="images/patient/atrisk2.png" class="fl fullwidth"/>
                            <h4 class="fl fullwidth desHwhite cntr_txt risk_lable">All pregnant and breastfeeding women</h4>
                        </div>
                        <div class="fl at_risk wow fadeInDown">
                            <img src="images/patient/atrisk3.png" class="fl fullwidth"/>
                            <h4 class="fl fullwidth desHwhite cntr_txt risk_lable">Older people aged 65 years and over</h4>
                        </div>
                        <div class="fr at_risk wow fadeInDown">
                            <img src="images/patient/atrisk4.png" class="fl fullwidth"/>
                            <h4 class="fl fullwidth desHwhite cntr_txt risk_lable">People with low or no exposure to the sun</h4>
                        </div>
                        <div class="fl at_risk wow fadeInDown">
                            <img src="images/patient/atrisk5.png" class="fl fullwidth"/>
                            <h4 class="fl fullwidth desHwhite cntr_txt risk_lable">People with darker skin</h4>
                        </div>
                        <div class="fr at_risk wow fadeInDown">
                            <img src="images/patient/atrisk6.png" class="fl fullwidth"/>
                            <h4 class="fl fullwidth desHwhite cntr_txt risk_lable">Obese individuals</h4>
                        </div>
                        <div class="fl at_risk wow fadeInDown">
                            <img src="images/patient/atrisk8.png" class="fl fullwidth"/>
                            <h4 class="fl fullwidth desHwhite cntr_txt risk_lable">Use of concomitant medication</h4>
                        </div>
                        <div class="fr at_risk wow fadeInDown">
                            <img src="images/patient/atrisk7.png" class="fl fullwidth"/>
                            <h4 class="fl fullwidth desHwhite cntr_txt risk_lable">Patient with malabsorption</h4>
                        </div>
                    </div>
                </div>
            </div>
             <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>