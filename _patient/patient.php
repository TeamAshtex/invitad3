<html>
<?php 
    include 'base/head.php';
?>
    <body class="patient_section">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                        <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>

                    <div class="fullwidth fl main_nav res_nav">
                        <?php include 'includes/patient/nav2.php';?>
                    </div>
                </div>
                <div class="patient_content fullwidth fl">
                    <div class="slider fl">
                        <!--Main Slider-->
                        <?php include 'includes/patient/main_slider.php';?>
                        <!--End Main Slider-->
                    </div>
                    <div class="main_content fl fullwidth wow fadeInDown">
                        <div class="boxes_d3 fl yellow_cta">
                            <a href="about.php" class="inner_d3 fl">
                                <img src="images/patient/cta1.jpg" class="v_mid d3_1"/>
                                <h3 class="v_mid">About vitamin D?</h3>
                            </a>
                        </div>
                        <div class="boxes_d3 fl grey_cta">
                            <a href="reminder.php" class="inner_d3 fl">
                                <img src="images/patient/cta2.jpg" class="v_mid d3_2"/>
                                <h3 class="v_mid">Your medicine reminder</h3>
                            </a>
                        </div>
                        <div class="boxes_d3 fl blue_cta last_cta">
                            <a href="leaflet.php" class="inner_d3 fl">
                                <img src="images/patient/cta3.jpg" class="v_mid d3_3"/>
                                <h3 class="v_mid">Download the package leaflet</h3>
                            </a>
                        </div>
                        <img src="images/patient/border_bottom.png" class="h_mid pa_btm_brdr"/>
                    </div>
                </div>
            </div>
            <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>