<html>
<?php 
    include 'base/head.php';
?>
    <body class="patient_section tabs_page">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid fullwidth">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                    <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>
                </div>
            </div>
            <div class="patient_content hcp_container h_mid">
                <div class="fl fullwidth res_about_us">
                    <div class="patient_container h_mid nav_container">
                        <div class="fullwidth fl main_nav res_nav">
                            <?php include 'includes/patient/nav2.php';?>
                        </div>
                        <img src="images/patient/tab1Res.png" alt="Talk To Your Doctor" class="h_mid"/>
                    </div>
                </div>
            </div>
            <!--Starting Tab Number 3's Detail content section "By Default its display block"-->
            <div class="fl fullwidth tabs-section tab1">
                <div class="fl fullwidth patient_container h_mid">
                    <h2 class="fl fullwidth desHe desHblack mar_0">
                        What InVita D3 is prescribed for
                    </h2>
                    <ul class="fl fullwidth marT_50">
                        <li class="main_det_li">
                            <img src="images/patient/tab1_1.png" class="fl"/>
                            <div class="tab_row_des fr">
                                <p class="fl desHe desHpink">
                                    50,000 IU/ml oral solution
                                </p>
                                <ul class="fl fullwidth desP desHgrey mar_0">
                                    <li>
                                        <p>InvitaD3 50,000 IU/ml oral solution is prescribed to treat vitamin D deficiency</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="main_det_li">
                            <img src="images/patient/tab1_2.png" class="fl"/>
                            <div class="tab_row_des fr">
                                <p class="fl desHe desHblue">
                                    InVita D3 25,000
                                </p>
                                <ul class="fl fullwidth desP desHgrey mar_0">
                                    <li class="sub_bullet">
                                        <p>InvitaD3 25,000 IU/ml oral solution is prescribed:</p>
                                        <span class="fl fullwidth">To treat vitamin D deficiency</span> 
                                        <span class="sub_or fl fullwidth">OR</span>
                                        <span class="fl fullwidth">
                                            To prevent vitamin D deficiency where it is felt that there 
                                            is an increased risk of vitamin D deficiency or the body is 
                                            facing an increased demand for vitamin D
                                        </span> 
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="main_det_li">
                            <img src="images/patient/tab3_2.png" class="fl"/>
                            <div class="tab_row_des fr">
                                <p class="fl desHe desHgreen">
                                    InVita D3 2,400 IU/ml oral drops, solution
                                </p>
                                <ul class="fl fullwidth desP desHgrey mar_0">
                                    <li>
                                        <p>
                                            To prevent vitamin D deficiency when it is felt that there is a significant risk of deficiency 
                                            or the body is facing an increased demand for vitamin D
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="main_det_li">
                            <img src="images/patient/tab3_3.png" class="fl"/>
                            <div class="tab_row_des fr">
                                <p class="fl desHe desHpinkL">
                                    InVita D3 800 IU soft gel capsules 
                                </p>
                                <ul class="fl fullwidth desP desHgrey mar_0">
                                    <li>
                                        <p>
                                            To prevent vitamin D deficiency where it is felt that there is an increased risk 
                                            of vitamin D deficiency or the body is facing an increased demand for vitamin D
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!--Ending Tab number 1's Detail content section "By default its display none"-->
            
            <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>