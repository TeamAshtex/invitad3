<head>
        <meta charset="UTF-8">
        <link href="css/all.css" type="text/css" rel="stylesheet" />
        <link href="css/res.css" type="text/css" rel="stylesheet" />
        <link href="css/animate.css" rel="stylesheet">
        <link rel="shortcut icon" href="images/patient/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/patient/favicon.ico" type="image/x-icon">

        <script src="js/jquery-1.11.3.min.js"></script>
        <script src="js/slider.js"></script>
        <script src="js/wow.js" type="text/javascript"></script>
		<script src="js/tabs.js" type="text/javascript"></script>
        <script src="js/multiple_functions.js" type="text/javascript"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            Invita
        </title>
</head>
<?php include 'includes/patient/res_nav.php';?>