<!DOCTYPE html>
<html>
    <?php 
    include 'base/head.php';
?>
    <body class="patient_section faq_page">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid fullwidth">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                        <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>
                </div>
            </div>
            <div class="patient_content hcp_container h_mid">
                <div class="fl bg_banner fullwidth">
                    <div class="patient_container h_mid nav_container">
                        <div class="fullwidth fl main_nav res_nav">
                            <?php include 'includes/patient/nav2.php';?>
                        </div>
                    </div>
                    <img src="images/patient/faq.png" class="h_mid treatment_banner"/>
                </div>
                <div class="patient_container fullwidth h_mid res_faq">
                    <div class="fl fullwidth faq_row wow fadeInDown">
                        <div class="fl thirtywidth">
                            <div class="causes_head fl fullwidth marB_20">
                                <h4 class="fl">Questions</h4>
                            </div>
                            <div class="fl fullwidth faq_sec faq_qu">
                                <h4>Are there any side effects
                                    I should be aware of?</h4>
                            </div>
                        </div>
                        <div class="fr sixty7width">
                            <div class="causes_head ans_head fl fullwidth marB_20">
                                <h4 class="fl">Answers</h4>
                            </div>
                            <div class="fl fullwidth faq_sec">
                                <p class=" fl desP desHgrey marB_20">
                                    As with any medicine, there may be side effects associated with taking InVita D3, 
                                    although not everybody gets them.
                                </p>
                                <p class="fl desP desHgrey marB_20">
                                    <b class="fl fullwidth">Possible side effects may include:</b>
                                    Possible side effects may include:
                                    <span class="fl fullwidth faq_bullet">Too much calcium in your blood (hypercalcaemia)</span>
                                    <span class="fl fullwidth faq_bullet">Too much calcium in your urine (hypercalciuria)</span>
                                </p>
                                <p class="fl desP desHgrey marB_20">
                                    <b class="fl fullwidth">Rare (affects less than 1 in 1000 people):</b>
                                    <span class="fl fullwidth faq_bullet">Skin rash</span>
                                    <span class="fl fullwidth faq_bullet">Itching</span>
                                    <span class="fl fullwidth faq_bullet">Hives</span>
                                </p>
                                <p class="fl desP desHgrey">
                                    Note: If you notice any kind of side effect after taking this medicine, talk to your doctor,
                                    pharmacist or nurse. This includes any possible side effects not listed in this leaflet. You can
                                    also report side effects directly via the Yellow Card Scheme at:
                                    <b class="fl fullwidth">https://yellowcard.mhra.gov.uk.</b>
                                    By reporting side effects, you can help provide more information on the safety of this medicine.
                                </p>
                            </div>                        
                        </div>
                    </div>

                    <div class="fl fullwidth c_sep faq_sep mar30_0 wow fadeInDown"></div>

                    <div class="fl fullwidth faq_row wow fadeInDown">
                        <div class="fl thirtywidth">
                            <div class="causes_head fl fullwidth marB_20 res_headOnly">
                                <h4 class="fl">Questions</h4>
                            </div>                            
                            <div class="fl fullwidth faq_sec faq_qu">
                                <h4>I’ve forgotten to
                                    take my usual dose.
                                    What should I do?</h4>
                            </div>
                        </div>
                        <div class="fr sixty7width">
                        <div class="causes_head ans_head fl fullwidth marB_20 res_headOnly">
                                <h4 class="fl">Answers</h4>
                            </div>
                            <div class="fl fullwidth faq_sec">
                                <p class=" fl desP desHgrey marB_20">
                                    If you forget to take a dose of InVita D3, take the forgotten dose as soon as possible. 
                                    Then take the next dose at the correct time. However, if it is almost time to take the next dose, 
                                    do not take the dose you have missed; just take the next dose as normal.
                                </p>
                                <p class="fl desP desHgrey marB_20">
                                    Note: do not take a double dose to make up for a forgotten dose.
                                </p>
                            </div>                        
                        </div>
                    </div>

                    <div class="fl fullwidth c_sep faq_sep mar30_0 wow fadeInDown"></div>

                    <div class="fl fullwidth faq_row wow fadeInDown">
                        <div class="fl thirtywidth">
                            <div class="causes_head fl fullwidth marB_20 res_headOnly">
                                <h4 class="fl">Questions</h4>
                            </div>
                            <div class="fl fullwidth faq_sec faq_qu">
                                <h4>I’ve forgotten to
                                    take my usual dose.
                                    What should I do?</h4>
                            </div>
                        </div>
                        <div class="fr sixty7width">
                        <div class="causes_head ans_head fl fullwidth marB_20 res_headOnly">
                                <h4 class="fl">Answers</h4>
                            </div>
                            <div class="fl fullwidth faq_sec">
                                <p class=" fl desP desHgrey marB_20">
                                    If you or your child takes more medicine than prescribed, do not take any more and contact 
                                    your doctor. If it is not possible to talk to a doctor, go to the nearest hospital emergency 
                                    department and take the medicine and all of its packaging with you.
                                </p>
                                <p class="fl desP desHgrey marB_20">
                                    The most common symptoms of overdose are: nausea, vomiting, excessive thirst, 
                                    the production of large amounts of urine over 24 hours, constipation and dehydration, 
                                    high levels of calcium in the blood (hypercalcaemia and hypercalciuria) shown by a 
                                    blood test. If you have any further questions on the use of this medicine, ask your doctor, 
                                    pharmacist or nurse.
                                </p>
                            </div>                        
                        </div>
                    </div>

                    <div class="fl fullwidth c_sep faq_sep mar30_0 wow fadeInDown"></div>

                    <div class="fl fullwidth faq_row wow fadeInDown">
                        <div class="fl thirtywidth">
                            <div class="causes_head fl fullwidth marB_20 res_headOnly">
                                <h4 class="fl">Questions</h4>
                            </div>
                            <div class="fl fullwidth faq_sec faq_qu">
                                <h4>What if I am pregnant?</h4>
                            </div>
                        </div>
                        <div class="fr sixty7width">
                            <div class="causes_head ans_head fl fullwidth marB_20 res_headOnly">
                                <h4 class="fl">Answers</h4>
                            </div>
                            <div class="fl fullwidth faq_sec">
                                <p class=" fl desP desHgrey marB_20">
                                    Vitamin D is very important to pregnant and breastfeeding women as they have 
                                    a higher need for it. There are doses of InVita D3 that can be taken by pregnant 
                                    and breastfeeding women, but if you are pregnant or thinking about becoming 
                                    pregnant, it’s best to talk to your doctor or pharmacist before taking InVita D3 
                                    or any other medicine. 
                                </p>
                            </div>                        
                        </div>
                    </div>

                </div>

            </div>
                        <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>