<!DOCTYPE html>
<html>
        <?php 
    include 'base/head.php';
?>
    <body class="hcp_section">
        <div class="wrapper h_mid fullwidth">
            <div class="hcp_container h_mid">
                <div class="fl fullwidth">
                    <?php include 'includes/hcp/top_search.php';?>
                </div>
                <div class="fl fullwidth">
                    <div class="container h_mid">
                        <div class="fr mobile_navigation">
                            <a href="javascript:void(0);" class="mobile_icon fr"></a>
                        </div>
                        <div class="navigation fr">
                            <?php include 'includes/hcp/nav1.php';?>
                        </div>
                        <?php include 'includes/hcp/logo.php';?>

                    </div>
                    <div class="fullwidth fl main_nav pos_rel">
                        <div class="container h_mid res_nav ">
                            <?php include 'includes/hcp/nav2.php';?>
                            <div class="fl fullwidth">
                                <h2 class="fl fullwidth main_heading marT_20">About Invita D3 range</h2>
                                <img src="images/hcp/intro_bg.jpg" class="fl"/>
                            </div>
                            <div class="halfwidth intro_top">
                                <p class="fl fullwidth desp">Vitamin D deficiency.</p>
                                <p class="fl fullwidth desp marB_20">We've got it covered</p>
                                <p class="fl fullwidth desp">UK licensed for vitamin D deficiency</p>
                                <p class="fl fullwidth desp marB_20">from <b class="desHpink">treatment</b> to <b class="desHblue">prevention</b></p>
                            </div>
                        </div>
                    </div>
                </div>   
            </div>
            <div class="patient_container h_mid hcp_content wow fadeInDown">
                <div class="fl intro_slider marT_50 pos_rel">
                    <a href="javascript:void(0);" onclick="plusDivs(-1)" class="slider_arrows arrow_left v_mid"></a>
                    <a href="javascript:void(0);" onclick="plusDivs(1)" class="slider_arrows arrow_right v_mid"></a>
                    <ul>
                        <li>
                            <img src="images/hcp/intro_slide1.png" class="h_mid mySlides"/>
                        </li>
                        <li>
                            <img src="images/hcp/intro_slide2.png" class="h_mid mySlides"/>
                        </li>
                        <li>
                            <img src="images/hcp/intro_slide3.png" class="h_mid mySlides"/>
                        </li>
                        <li>
                            <img src="images/hcp/intro_slide4.png" class="h_mid mySlides"/>
                        </li>
                    </ul>
                </div>
                <div class="fr intro_right marT_50">
                    <h4 class="fl fullwidth desHorg">Explore further..</h4>
                    <ul class="fl fullwidth right_intro_cta">
                        <li><a href="javascript:void(0);">Who can take it?</a></li>
                        <li><a href="javascript:void(0);">How to take invita D3</a></li>
                        <li><a href="javascript:void(0);">Dosing frequency</a></li>
                    </ul>
                </div>
            </div>
            
            <!--Footer-->
            <?php include 'includes/hcp/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/hcp/footer2.php';?>
            <!--End Footer Bottom-->

        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/hcp/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>