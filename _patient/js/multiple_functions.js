$(document).ready(function () {

    //Find and Replace the imgaes on healthrisks page
    if ($(window).width() < 770) {
        $('img[src="images/patient/fig_brain.png"]').attr('src', 'images/patient/fig_brainRes.png');
        $('img[src="images/patient/fig_circularity.png"]').attr('src', 'images/patient/fig_circularityRes.png');
        $('img[src="images/patient/fig_muscle.png"]').attr('src', 'images/patient/fig_muscleRes.png');

        $(".fig_plus").click(function () {
            $(".fig_plus").each(function () {
                $(this).parent().find(".fig_det").removeClass("fig_vis");
                $(this).removeClass("fig_cross");
            });
        });
    }

    //Health Risks page animation
    $(".fig_plus").click(function () {
        $(this).parent().find(".fig_det").toggleClass("fig_vis");
        $(this).toggleClass("fig_cross");
    });


    //Responsive navigation
    $(".mobile_icon").click(function () {
        $(".resposnsive_menu").slideDown("slow");
    });
    $(".close_nav_icon").click(function () {
        $(".resposnsive_menu").slideUp("slow");
    });
    $(".for_res_subnav").click(function () {
        $(".res_subnav").slideToggle("slow");
    });
    $(".for_res_subnav2").click(function () {
        $(".res_subnav2").slideToggle("slow");
    });
    $(".for_res_subnav2").click(function () {
        $(".res_subnav").slideUp("slow");
    });
    $(".for_res_subnav").click(function () {
        $(".res_subnav2").slideUp("slow");
    });

    
    $(function () {
        var current_location = window.location.href;
        current_location = current_location.split("/");
        current_location = current_location[current_location.length - 1];
        $(".my_menu").find("a").each(function () {
            if ($(this).attr("href").indexOf(current_location) >= 0) {
                $(this).addClass("active");
            }
        });
    });
});