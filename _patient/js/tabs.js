$(document).ready(function () {
    $("li a.faq1, li a.faq2, li a.faq3, li a.faq4").click(function () {
        var current_tab = $(this).attr("data-tab");
        $("li a.faq1, li a.faq2, li a.faq3, li a.faq4").removeClass("active");
        $("li.for_tab, div.tabs-section, h2.tabs-section").hide();
        $("li.for_tab.for_" + current_tab + ", div.tabs-section."  + current_tab + ", h2.tabs-section."  + current_tab).show();
        $(this).addClass("active");
    });
});


//


$(document).ready(function () {
    if ($(window).width() > 1060) {
        $(".boost_box1, .boost_box2, .boost_box3").click(function () {
            var current_tab = $(this).attr("data-tab");
            $(".boost_box1, .boost_box2, .boost_box3").removeClass("actived_boost");
            $(".inner_boostdet.boost_tab1, .inner_boostdet.boost_tab2, .inner_boostdet.boost_tab3").hide();
            $(".inner_boostdet.boost_" + current_tab).show();
            $(this).addClass("actived_boost");
        });
    }
});