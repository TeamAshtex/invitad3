<!DOCTYPE html>
<html>
    <?php 
    include 'base/head.php';
?>
    <body class="patient_section treatment_page">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid fullwidth">
                <div class="patient_header fl fullwidth">
                   <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                        <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>
                </div>
            </div>
            <div class="patient_content hcp_container h_mid">
                <div class="fl bg_banner fullwidth">
                    <div class="patient_container h_mid nav_container">
                        <div class="fullwidth fl main_nav res_nav">
                            <?php include 'includes/patient/nav2.php';?>
                        </div>
                    </div>
                    <img src="images/patient/causes.jpg" class="h_mid treatment_banner"/>
                </div>
                <div class="patient_container fullwidth h_mid treatment_det cause_sec">
                    <div class="causes_head fl fullwidth">
                        <h4 class="fl">The Causes</h4>
                    </div>
                    <div class="fl fullwidth causes_det">
                        <div class="fl fullwidth wow fadeInDown">
                            <div class="fl seventywidth">
                                <p class="fl fullwidth desP desHgrey">
                                    Our main source of vitamin D is from sunlight on our skin. Because of this, the most common cause of 
                                    vitamin D deficiency is a lack of sunlight. Vitamin D deficiency is most common in people who don’t spend 
                                    much time outdoors, such as older people and people who live in nursing homes. 
                                </p>
                                <div class="fl fullwidth c_sep"></div>
                            </div>
                            <div class="fl thirtywidth">
                                <img src="images/patient/cause1.png" class="fl c_img"/>
                                <img src="images/patient/cause1res.png" class="fl res_c_img"/>
                            </div>
                        </div>
                        <div class="fl fullwidth wow fadeInDown">
                            <div class="fl seventywidth">
                                <p class="fl fullwidth desP desHgrey">
                                    Our main source of vitamin D is from sunlight on our skin. Because of this, the most common cause of 
                                    vitamin D deficiency is a lack of sunlight. Vitamin D deficiency is most common in people who don’t spend 
                                    much time outdoors, such as older people and people who live in nursing homes. 
                                </p>
                                <div class="fl fullwidth c_sep"></div>
                            </div>
                            <div class="fl thirtywidth">
                                <img src="images/patient/cause2.png" class="fl c_img"/>
                                <img src="images/patient/cause2res.png" class="fl res_c_img"/>
                            </div>
                        </div>
                        <div class="fl fullwidth wow fadeInDown">
                            <div class="fl seventywidth">
                                <p class="fl fullwidth desP desHgrey">
                                    Our main source of vitamin D is from sunlight on our skin. Because of this, the most common cause of 
                                    vitamin D deficiency is a lack of sunlight. Vitamin D deficiency is most common in people who don’t spend 
                                    much time outdoors, such as older people and people who live in nursing homes. 
                                </p>
                                <div class="fl fullwidth c_sep"></div>
                            </div>
                            <div class="fl thirtywidth">
                                <img src="images/patient/cause3.png" class="fl c_img"/>
                                <img src="images/patient/cause3res.png" class="fl res_c_img"/>
                            </div>
                        </div>
                        <div class="fl fullwidth wow fadeInDown">
                            <div class="fl seventywidth">
                                <p class="fl fullwidth desP desHgrey">
                                    Our main source of vitamin D is from sunlight on our skin. Because of this, the most common cause of 
                                    vitamin D deficiency is a lack of sunlight. Vitamin D deficiency is most common in people who don’t spend 
                                    much time outdoors, such as older people and people who live in nursing homes. 
                                </p>
                                <div class="fl fullwidth c_sep"></div>
                            </div>
                            <div class="fl thirtywidth">
                                <img src="images/patient/cause4.png" class="fl c_img"/>
                                <img src="images/patient/cause4res.png" class="fl res_c_img"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                         <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>