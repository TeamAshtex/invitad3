<!DOCTYPE html>
<html>
           <?php 
    include 'base/head.php';
?>
    <body class="patient_section syptoms_page">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid fullwidth">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                        <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>
                </div>
            </div>
            <div class="patient_content hcp_container h_mid">
                <div class="fl bg_banner fullwidth">
                    <div class="patient_container h_mid nav_container">
                        <div class="fullwidth fl main_nav res_nav">
                            <?php include 'includes/patient/nav2.php';?>
                        </div>
                    </div>
                </div>
                <div class="patient_container tabs_pos fullwidth h_mid">
                    <h2 class="fl fullwidth main_heading">
                        Cookies
                    </h2>
                </div>
            </div>
            <div class="patient_container h_mid fullwidth">
                <h2 class="fl fullwidth main_heading heading_blue">
                    Introduction
                </h2>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    This website uses cookies so that we can provide you with the best user experience. Without some of these cookies, the website simply would not work. Some of the cookies used on this site perform functions like recognising you each time you visit the site or help understand which parts of the site you find most interesting and useful. We have created this separate cookies policy in order to provide comprehensive information about Consilient Health website's use of cookies and what cookies are.
                </p>
                <h2 class="fl fullwidth main_heading marT_50 heading_blue">
                    What is a cookie?
                </h2>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    A cookie is a small amount of data, which often includes a unique identifier that is sent to your computer or mobile device (referred to here as a "device") browser from a website's server and is stored on your device's hard drive. Each website or third party service provider used by the website can send its own cookie to your browser if your browser's preferences allow it, but (to protect your privacy) your browser only permits a website or third party service provider to access the cookies it has already sent to you, not the cookies sent to you by other sites or other third party service providers. A cookie will contain some anonymous information such as a unique identifier and the site name and some digits and numbers. It allows a website to remember things like your preferences.
                </p>
                <h2 class="fl fullwidth main_heading marT_50 heading_blue">
                    What is a browser?
                </h2>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    A browser is an application that allows you to surf the internet. The most common browsers are Chrome, Internet Explorer, Firefox and Safari. Most browsers are secure and offer quick and easy ways to delete information like cookies. Please see the section below 'Change your Browser Settings'.
                </p>
                <h2 class="fl fullwidth main_heading marT_50 heading_blue">
                    What do cookies do?
                </h2>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Cookies record information about your online preferences and allow us to tailor the websites to your interests. Information supplied by cookies can help us to analyse your use of our sites and help us to provide you with a better user experience. For example, you may choose to personalise the content of a website in order to see the latest news and weather for your region. In order to do this, a cookie is placed on your device to remember where you live so that we deliver the information that has been requested by you. This is a prime example of how cookies are used to improve your experience of a website.
                </p>
                <h2 class="fl fullwidth main_heading marT_50 heading_blue">
                    Change your Browser Settings
                </h2>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    You can choose how cookies are handled by your device via your browser settings. The most popular browsers allow users to a) accept all cookies, b) to notify you when a cookie is issued, or c) to not receive cookies at any time. If you choose not to receive cookies at any time, the website may not function properly and certain services will not be provided, spoiling your experience of the website. Each browser is different, so check the "Help" menu of your browser to learn how to change your cookie preferences.
                </p>
                <h2 class="fl fullwidth main_heading marT_50 heading_blue">
                    Types of Cookie
                </h2>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    First Party Cookies 
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    First party cookies are set by the website you are visiting and they can only be read by that site. 
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Third Party Cookies 
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Third party cookies are set by other organisations that we use for different services. For example, the Consilient Health website uses external analytics services and these suppliers may set cookies on the website's behalf in order to report what's popular and what's not. To learn more and to exercise control over such cookies, see the below section on "Analytics on our Website". 
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Session Cookies 
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Session Cookies are stored only for the duration of your visit to a website and these are deleted from your device when your browsing session ends. 
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Persistent Cookies 
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    This type of cookie is saved on your device for a fixed period. Persistent cookies are used where we need to know who you are for more than one usage session. For example, if you have asked us to remember preferences like your username.
                </p>
                <h2 class="fl fullwidth main_heading marT_50 heading_blue">
                    How do we use cookies?
                </h2>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    We use cookies to help us understand how users are using our site.
                </p>
                <h2 class="fl fullwidth main_heading marT_50 heading_blue">
                    Analytics on our Website
                </h2>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Google Analytics provide anonymised statistical information for us. They process IP addresses and information from cookies used on our sites so we know how many page views we have, how many users we have, what browsers they are using (so we can target our resources in the right way to maximise compatibility for the majority of our users) and, in some cases, city or region they are located.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    If you would like to opt out of Analytics cookies, please do so by clicking on the link below: 
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Google Analytics: https://tools.google.com/dlpage/gaoptout
                </p>
            </div>
            <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>