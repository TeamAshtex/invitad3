<!DOCTYPE html>
<html>
    <?php 
    include 'base/head.php';
?>
    <body class="patient_section whatIsIt">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid fullwidth">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                        <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>
                </div>
            </div>
            <div class="patient_content hcp_container h_mid">
                <div class="fl bg_banner fullwidth">
                    <div class="patient_container h_mid nav_container">
                        <div class="fullwidth fl main_nav res_nav">
                            <?php include 'includes/patient/nav2.php';?>
                        </div>
                    </div>
                </div>
                <div class="patient_container tabs_pos fullwidth h_mid">
                    <h2 class="fl fullwidth main_heading">
                        About Vitamin D - What is it?
                    </h2>
                    <div class="fl fullwidth wow fadeInDown marT_30">
                        <div class="fl boost_box" data-tab="tab1">
                            <div class="inner_boost h_mid">
                                <img src="images/patient/wii_bone.png" class="h_mid"/>
                                <div class="fl fullwidth">
                                    <h4 class="fl fullwidth cntr_txt heading_blue mar20_0">
                                        For strong healthy bones
                                    </h4>
                                    <p class="fullwidth cntr_txt desP desHgrey wii_p">
                                        Vitmin D is a vitamin that is needed for good health. It helps to calcium and phosphorous in our diet be absorbed - these
                                        are needed to keep our bones healthy and strong.
                                    </p>
                                </div>
                            </div>
                            <figure class="boost_icon"></figure>
                        </div>
                        <div class="fl boost_box" data-tab="tab1">
                            <div class="inner_boost h_mid">
                                <img src="images/patient/wii_muscle.png" class="h_mid"/>
                                <div class="fl fullwidth">
                                    <h4 class="fl fullwidth cntr_txt heading_blue mar20_0">
                                        For muscles and general health
                                    </h4>
                                    <p class="fullwidth cntr_txt desP desHgrey wii_p">
                                        Vitmin D is also thought to be important for muscles and general health.
                                    </p>
                                </div>
                            </div>
                            <figure class="boost_icon"></figure>
                        </div>
                        <div class="fl boost_box boostbox_last" data-tab="tab1">
                            <div class="inner_boost h_mid">
                                <img src="images/patient/wii_sun.png" class="h_mid"/>
                                <div class="fl fullwidth">
                                    <h4 class="fl fullwidth cntr_txt heading_blue mar20_0">
                                        Produced by sunlight exposure
                                    </h4>
                                    <p class="cntr_txt desP desHgrey wii_p">
                                        Vitamin D is mostly made in the skin by exposure to sunlight - very little of our natural diet contains vitamin 
                                        D, although some foods are fortified (enriched) with it.
                                    </p>
                                </div>
                            </div>
                        </div>
<div class="h_mid boost_box_res">
                            <div class="fl fullwidth">
                                <h4 class="fl fullwidth cntr_txt heading_blue mar20_0">
                                    For strong healthy bones
                                </h4>
                                <p class="fullwidth cntr_txt desP desHgrey wii_p">
                                    Vitmin D is a vitamin that is needed for good health. It helps to calcium and phosphorous in our diet be absorbed - these
                                    are needed to keep our bones healthy and strong.
                                </p>
                            </div>
                            <div class="fl fullwidth">
                                <h4 class="fl fullwidth cntr_txt heading_blue mar20_0">
                                    For muscles and general health
                                </h4>
                                <p class="fullwidth cntr_txt desP desHgrey wii_p">
                                    Vitmin D is also thought to be important for muscles and general health.
                                </p>
                            </div>
                            <div class="fl fullwidth">
                                <h4 class="fl fullwidth cntr_txt heading_blue mar20_0">
                                    Produced by sunlight exposure
                                </h4>
                                <p class="cntr_txt desP desHgrey wii_p">
                                    Vitamin D is mostly made in the skin by exposure to sunlight - very little of our natural diet contains vitamin 
                                    D, although some foods are fortified (enriched) with it.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="fl fullwidth wow fadeInDown marT_20">
                        <div class="wi_defi h_mid">
                            <h4 class="fl fullwidth heading_orng mar20_0 lightH_25">
                                What is vitamin D deficiency?
                            </h4>
                            <p class="fl fullwidth desP desHgrey">
                                Vitamin D deficiency means you have a low level of vitamin D in your body.
                                If your doctor suspects you are deficient in vitamin D, they may do a blood test 
                                as part of the diagnosis. The results of this test will show if you have vitamin D 
                                deficiency. Some people are more at risk of developing vitamin D deficiency than 
                                others. For example, people over 65, dark-skinned people, pregnant and breast 
                                feeding women are more at risk. If your doctor thinks that you are at risk of 
                                vitamin D deficiency, they may decide to treat you to prevent this from happening. 
                            </p>
                        </div>
                    </div>
                    <div class="fl fullwidth mar_0 top_bg">
                        <div class="fl fullwidth wow fadeInDown">
                            <div class="fl wi_boxes">
                                <h4 class="fl heading_blue">Who is at risk?</h4>   
                                <p class="fl fullwidth desP desHgrey marT_20">
                                    There are a number of groups of people who are considered to be more at risk than others of developing vitamin D deficiency
                                </p>
                            </div>
                            <div class="fl wi_boxes">
                                <h4 class="fl heading_blue">Symptoms & Diagnosis</h4>   
                                <p class="fl fullwidth desP desHgrey marT_20">
                                    Vitamin D deficiency can be confirmed with a simple blood test
                                </p>
                            </div>
                            <div class="fl wi_boxes">
                                <h4 class="fl heading_blue">Boosting your vitamin D</h4>   
                                <p class="fl fullwidth desP desHgrey marT_20">
                                    There are a few ways you can help to boost your levels of vitamin D
                                </p>
                            </div>
                        </div>
                        <div class="fl fullwidth c_sep mar30_0"></div>
                        <div class="fl fullwidth wow fadeInDown">
                            <div class="fl wi_boxes">
                                <h4 class="fl heading_blue">Treatments</h4>   
                                <p class="fl fullwidth desP desHgrey marT_20">
                                    Your doctor may decide to prescribe a vitamin D medicine to prevent or treat your symptoms
                                </p>
                            </div>
                            <div class="fl wi_boxes">
                                <h4 class="fl heading_blue">What causes deficiency?</h4>   
                                <p class="fl fullwidth desP desHgrey marT_20">
                                    Learn more about what can cause vitamin D deficiency 
                                </p>
                            </div>
                            <div class="fl wi_boxes">
                                <h4 class="fl heading_blue">Health risk</h4>   
                                <p class="fl fullwidth desP desHgrey marT_20">
                                    Some of the health risk associated with untreated vitamin D deficiency affect many parts of the body
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>