<!DOCTYPE html>
<html>
        <?php 
    include 'base/head.php';
?>
    <body class="patient_section syptoms_page">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid fullwidth">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                        <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>
                </div>
            </div>
            <div class="patient_content hcp_container h_mid">
                <div class="fl bg_banner fullwidth">
                    <div class="patient_container h_mid nav_container">
                        <div class="fullwidth fl main_nav res_nav">
                            <?php include 'includes/patient/nav2.php';?>
                        </div>
                    </div>
                </div>
                <div class="patient_container tabs_pos fullwidth h_mid">
                    <h2 class="fl fullwidth main_heading">
                        Terms and Conditions
                    </h2>
                </div>
            </div>
            <div class="patient_container h_mid fullwidth">
                <p class="fl fullwidth desP2 desHgrey mar_0">
                    Welcome to the InVita D3 website. If you continue to browse and use this website you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern Consilient Health’s relationship with you in relation to this website.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    The term Consilient Health or ‘us’ or ‘we’ refers to the owner of the website whose registered office is 5th Floor, Beaux Lane House, Mercer Street Lower, Dublin 2, Ireland. Our company registration number is No: 347231, registered in Ireland. The term ‘you’ refers to the user or viewer of our website.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    The use of this website is subject to the following terms of use: The content of the pages of this website is for your general information and use only. It is subject to change without notice.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    All trademarks reproduced in this website, which are not the property of, or licensed to the operator, are acknowledged on the website. Unauthorised use of this website may give rise to a claim for damages and/or be a criminal offence.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    You may not create a link to this website from another website or document without Consilient Health’s prior written consent.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Your use of this website and any dispute arising out of such use of the website is subject to the laws of England, Scotland and Wales.
                </p>
                <h2 class="fl fullwidth main_heading marT_50 heading_blue">
                    Privacy Policy
                </h2>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Consilient Health Limited and Consilient Health (UK) Limited ("We") are committed to protecting and respecting your privacy.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    This policy (together with any other documents referred to in it) sets out the basis on which any personal data (including sensitive personal data) we collect from you, or that you provide to us, will be processed by us. Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    For the purpose of the Data Protection Acts 1988 and 2003 (the "Act"), Consilient Health Limited of [5th Floor, Beaux Lane House, Mercer Street Lower, Dublin 2] is a data controller.
                </p>
                <h4 class="fl fullwidth main_heading marT_20 heading_blue">
                    1.INFORMATION WE MAY COLLECT FROM YOU
                </h4>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    1.1 We may collect and process the following data about you:
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    1.1.1 Information you give us. You may give us information about you by filling in order forms or by corresponding with us by phone, e-mail or otherwise. This includes information you provide when you use our services, place an order, attend one of our events, or when you contact us about one of our products. The information you give us may include your name, address, e-mail address and phone number, financial and credit card information, date of birth, gender, and location from where you are calling, and may also include sensitive personal data such as information about your physical or mental health or condition in the event you are contacting us about one of our products. Healthcare professionals may also provide us with information relating to their specialities and professional affiliations.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    1.1.2 Information we receive from other sources. We may receive information about you if you use any of the other services we provide. We are also working closely with third parties (including, for example, business partners, service providers, sub-contractors in technical, payment and delivery services, analytics providers, credit reference agencies) and may receive information about you from them.
                </p>
                <h4 class="fl fullwidth main_heading marT_20 heading_blue">
                    2. USES MADE OF THE INFORMATION
                </h4>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    2.1 We use information held about you in the following ways:
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    2.1.1 Information you give to us. We will use this information:
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    (a) to carry out our obligations arising from any contracts entered into between you and us and to provide you with the information, products and services that you request from us;
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    (b) to provide you with information about other goods and services we offer that are similar to those that you have already purchased or enquired about;
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    (c) to provide you, or permit selected third parties to provide you, with information about goods or services we feel may interest you. If you are an existing customer, we will only contact you by electronic means (e-mail or SMS) with information about goods and services similar to those which were the subject of a previous sale or negotiations of a sale to you. If you are a new customer, and where we permit selected third parties to use your data, we (or they) will contact you by electronic means only if you have consented to this. If you do not want us to use your data in this way, or to pass your details on to third parties for marketing purposes, please tick the relevant box situated on the form on which we collect your data (the order form) or contact us on the details provide in the "Your Rights" section below;
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    (d) to notify you about changes to our services or products;
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    (e) to validate your ability to access or use certain products, services or information (e.g. some of our products are only suitable for or accessible by individuals meeting certain eligibility or other criteria such as being a licensed healthcare professional or holding a wholesale dealer licence);
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    (f) to provide customer service and support.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    2.1.2 Information we receive from other sources. We may combine this information with information you give to us and information we collect about you. We may use this information and the combined information for the purposes set out above (depending on the types of information we receive).
                </p>
                <h4 class="fl fullwidth main_heading marT_20 heading_blue">
                    3. WEBSITE USE
                </h4>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    3.1 In relation to any data we collect from you by visiting http://www.vitamin-d3.co.uk/ (the"site") you are accepting and consenting to the practices described in this policy and our terms of use (http://www.vitamin-d3.co.uk/terms-conditions).
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    3.2 With regard to each of your visits to our site, we may automatically collect and process the following information:
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    3.2.1 technical information, including the Internet protocol (IP) address used to connect your computer to the Internet, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform;
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    3.2.2 information about your visit, including the full Uniform Resource Locators (URL) clickstream to, through and from our site (including date and time); products you viewed or searched for; page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), and methods used to browse away from the page and any phone number used to call our customer service number.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    3.3 We may also receive information about you if you use any of the other websites we operate.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    3.4 We will use this information:
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    3.4.1 to administer our site and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes;
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    3.4.2 to improve our site to ensure that content is presented in the most effective manner for you and for your computer;
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    3.4.3 to allow you to participate in interactive features of our service, when you choose to do so;
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    3.4.4 as part of our efforts to keep our site safe and secure;
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    3.4.5 to measure or understand the effectiveness of advertising we serve to you and others, and to deliver relevant advertising to you;
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    3.4.6 to make suggestions and recommendations to you and other users of our site about goods or services that may interest you or them
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    3.5 Our site may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies. Please check these policies before you submit any personal data to these websites.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    3.6 Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.
                </p>
                <h4 class="fl fullwidth main_heading marT_20 heading_blue">
                    4. COOKIES
                </h4>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Our site uses cookies to distinguish you from other users of our site. This helps us to provide you with a good experience when you browse our site and also allows us to improve our site. For detailed information on the cookies we use and the purposes for which we use them see our cookies policy available at www.vitamin-d3.co.uk/cookies.
                </p>
                <h4 class="fl fullwidth main_heading marT_20 heading_blue">
                    5. MOBILE APPLICATION SOFTWARE
                </h4>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    In the event that you use any of our mobile application software, we will process any data that we collect from you or which you provide to us (such as your name, e-mail address, and device type) in accordance with this privacy policy. In particular, we may use information we hold about you for the purposes of processing and responding to any questions you submit to us about our mobile application software. Any use of our mobile application software will also be subject to any terms and conditions provided to you at the time of download of the mobile application software.
                </p>
                <h4 class="fl fullwidth main_heading marT_20 heading_blue">
                    6. DISCLOSURE OF YOUR INFORMATION
                </h4>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    6.1 We may share your personal information with any member of our group, which means our subsidiaries, our ultimate holding company and its subsidiaries, as defined in the Irish Companies Act 1963.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    6.2 We may share your information with selected third parties including:
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    6.2.1 Business partners, service providers, suppliers and sub-contractors for the performance of any contract we enter into with you or them;
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    6.2.2 In relation to use of our site, analytics and search engine providers that assist us in the improvement and optimisation of our site;
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    6.2.3 Credit reference agencies for the purpose of assessing your credit score where this is a condition of us entering into a contract with you.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    6.3 We may disclose your personal information to third parties:
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    6.3.1 In the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets;
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    6.3.2 If Consilient Health Limited and/or Consilient Health (UK) Limited or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets;
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    6.3.3 In order to comply with the industry codes of practice, including the ABPI Code of Practice for the Pharmaceutical Industry and the EFPIA Code. For example, we are required to document and publically disclose certain transfers of value made to health professionals and healthcare organisations;
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    6.3.4 If we are under a duty to disclose or share your personal data in order to comply with any legal obligation or industry best practice standards, or in order to enforce or apply our terms of use or terms and conditions of supply and other agreements; or to protect the rights, property, or safety of Consilient Health Limited and/or Consilient Health (UK) Limited, our customers, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.
                </p>
                <h4 class="fl fullwidth main_heading marT_20 heading_blue">
                    7. WHERE WE PROCESS YOUR PERSONAL DATA
                </h4>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    The data that we collect from you may be transferred to, and processed at, a destination outside the European Economic Area ("EEA"). It may also be processed by staff operating outside the EEA who work for us or for one of our suppliers. Such staff maybe engaged in, among other things, the fulfilment of your order, the processing of your payment details and the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.
                </p>
                <h4 class="fl fullwidth main_heading marT_20 heading_blue">
                    8. YOUR RIGHTS
                </h4>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    You have the right to ask us not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by checking certain boxes on the forms we use to collect your data. You can also exercise the right at any time by contacting us at info@consilienthealth.com.
                </p>
                <h4 class="fl fullwidth main_heading marT_20 heading_blue">
                    9. ACCESS TO INFORMATION
                </h4>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    The Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. Any access request may be subject to a fee of £10 to meet our costs in providing you with details of the information we hold about you.
                </p>
                <h4 class="fl fullwidth main_heading marT_20 heading_blue">
                    10. CHANGES TO OUR PRIVACY POLICY
                </h4>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Any changes we may make to our privacy policy in the future will be posted on this page and, where appropriate, notified to you by e-mail. Please check back frequently to see any updates or changes to our privacy policy.
                </p>
                <h4 class="fl fullwidth main_heading marT_20 heading_blue">
                    11. CONTACT
                </h4>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to info@consilienthealth.com.
                </p>
                <h2 class="fl fullwidth main_heading marT_50 heading_blue">
                    Website Disclaimer
                </h2>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    The information contained in this website is for general information purposes only. The information is provided by Consilient Health and while we endeavour to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of this website.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Through this website you are able to link to other websites which are not under the control of Consilient Health. We have no control over the nature, content and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Every effort is made to keep the website up and running smoothly. However, Consilient Health takes no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control.
                </p>
                <h2 class="fl fullwidth main_heading marT_50 heading_blue">
                    Links to other websites 
                </h2>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Our website may contain links to other websites of interest. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information, which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website in question.
                </p>
                <p class="fl fullwidth desP2 desHgrey marT_20">
                    Consilient Health may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from 16th July 2015.
                </p>
            </div>
            <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>