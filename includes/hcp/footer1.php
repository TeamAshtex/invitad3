<div class="patient_footer fullwidth fl wow fadeInDown">
                <div class="thik_border fullwidth fl"></div>
                <div class="inner_pfooter h_mid">
                    <div class="patient_container h_mid">
                        <div class="halfwidth fl pos_rel in_foot">
                            <div class="halfwidth fl">
                                <ul>
                                    <li>
                                        <a href="javascript:void(0);">About InVita D3</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">How to take InVita D3</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Acquisition Costs</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Consilient Health</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">NOS Guidelines</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Prescribing Information</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="halfwidth fl">
                                <ul>
                                    <li>
                                        <a href="javascript:void(0);">Home</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Contact Us</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Sitemap</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Terms and Conditions</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Cookies</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="v_border"></div>
                        </div>
                        <div class="halfwidth fl in_foot">
                            <div class="fullwidth fl note_section">
                                <div class="notice_icon fl">
                                    <img src="images/hcp/notice_icon.png"/>
                                </div>
                                <div class="notice_area fl">
                                    <h6>Reporting of side effects:</h6> 
                                    <p>
                                        Adverse events should be reported. Reporting forms and information can 
                                        be found at https://yellowcard.mhra.gov.uk/. Adverse events should also 
                                        be reported to Consilient Health (UK) Ltd, Ground Floor, No. 1 Church Road, 
                                        Richmond upon Thames, Surrey TW9 2QE UK or 
                                        drugsafety@consilienthealth.com.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="h_mid mobile_footer">
                            <ul>
                                <li><a href="javascript:void(0);">Home</a></li>
                                <li><a href="javascript:void(0);">Contact Us</a></li>
                                <li><a href="javascript:void(0);">Site Map</a></li>
                                <li><a href="javascript:void(0);">Terms and Conditions</a></li>
                                <li><a href="javascript:void(0);">Cookies</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="thik_border fullwidth fl"></div>
            </div>