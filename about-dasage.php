<html>
<?php 
    include 'base/head.php';
?>
    <body class="patient_section tabs_page tabs_page2">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid fullwidth">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                    <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>
                </div>
            </div>
            <div class="patient_content hcp_container h_mid">
                <div class="fl fullwidth res_about_us">
                    <div class="patient_container h_mid nav_container">
                        <div class="fullwidth fl main_nav res_nav">
                            <?php include 'includes/patient/nav2.php';?>
                        </div>
                        <img src="images/patient/tab2Res.png" alt="Talk To Your Doctor" class="h_mid"/>
                    </div>
                </div>
            </div>
            <!--Starting Tab number 2's Detail content section "By default its display none"-->

            <div class="fl fullwidth tabs-section">
                <div class="fl fullwidth tab2_bg">
                    <div class="fl fullwidth patient_container h_mid">
                        <p class="fl fullwidth main_heading desP desHgrey marB_50">
                            Your doctor will discuss the best dose and treatment schedule for you  this will be determined by your situation, age and the severity of the deficiency. Always take your medicine as your doctor has prescribed it.
                        </p>
                        <h2 class="fl fullwidth main_heading desHe desHblue">
                            Treatment
                        </h2>
                        <div class="tab2_det1 fl fullwidth">
                            <div class="tab2_tratment">
                                <img src="images/patient/treatment1.png" class="dosage_img"/>
                                <img src="images/patient/treatment1Res.png" class="h_mid res_dosage"/>
                            </div>
                            <div class="tab2_tratment">
                                <img src="images/patient/treatment2.png" class="dosage_img"/>
                                <img src="images/patient/treatment2Res.png" class="h_mid res_dosage"/>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:void(0);" class="h_mid dwn_icon"></a>
                </div>
                <div class="fl fullwidth tab2_bg2">
                    <div class="fl fullwidth patient_container h_mid">
                        <h2 class="fl fullwidth main_heading desHe desHblue">
                            Prevention
                        </h2>
                        <div class="tab2_det1 fl fullwidth">
                            <div class="tab2_tratment">
                                <img src="images/patient/Prevention1.png" class="dosage_img"/>
                                <img src="images/patient/Prevention1Res.png" class="h_mid res_dosage"/>
                            </div>
                            <div class="tab2_tratment">
                                <img src="images/patient/Prevention2.png" class="dosage_img"/>
                                <img src="images/patient/Prevention2Res.png" class="h_mid res_dosage"/>
                            </div>
                            <div class="tab2_tratment">
                                <img src="images/patient/Prevention3.png" class="dosage_img"/>
                                <img src="images/patient/Prevention3Res.png" class="h_mid res_dosage"/>
                            </div>
                            <div class="tab2_tratment">
                                <img src="images/patient/Prevention4.png" class="dosage_img"/>
                                <img src="images/patient/Prevention4Res.png" class="h_mid res_dosage"/>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:void(0);" class="h_mid dwn_icon"></a>
                </div>
                <div class="fl fullwidth tab2_bg tab2_bg_last">
                    <div class="fl fullwidth patient_container h_mid">
                        <h2 class="fl fullwidth main_heading desHe desHgreen">
                            InVita D3 dose – infants and children
                        </h2>
                        <div class="tab2_det1 fl fullwidth">
                            <div class="tab2_tratment">
                                <img src="images/patient/childern1.png" class="dosage_img"/>
                                <img src="images/patient/childern1Res.png" class="h_mid res_dosage"/>
                            </div>
                        </div>
                        <a href="javascript:void(0);" class="fl desHgrey btt_top">Back to the top</a>
                    </div>
                </div>
            </div>
            <!--Ending Tab number 2's Detail content section "By default its display none"-->
            
            <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>