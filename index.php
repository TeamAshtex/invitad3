<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="css/all.css" type="text/css" rel="stylesheet" />
        <link href="css/res.css" type="text/css" rel="stylesheet" />
        <script src="js/jquery-1.11.3.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            Invita
        </title>
    </head>
    <body>
        <div class="wrapper h_mid fullwidth">
            <div class="outer_container_splash h_mid">
                <div class="inner_container_splash h_mid">
                    <div class="splash_header fl fullwidth">
                        <a href="index.html">
                            <img src="images/logo.png" alt="logo" class="h_mid"/>
                        </a>
                    </div>
                    <div class="splash_content fl fullwidth">
                        <div class="splash_inner_content h_mid">
                            <div class="splash_inner_left fl">
                                <!--                                <img src="images/healthcare.png" class="selection_heading fl"/>-->
                                <h1 class="selection_heading fl resp_heading">Are you a Healthcare professional</h1>
                                <div class="fr resp_switch">
                                    <a href="_hcp/home.php" class="selection_link fl"></a>
                                    <span class="border orange fr"></span>
                                </div>
                            </div>
                            <div class="splash_inner_right fr">
                                <!--                                <img src="images/prescribed.png" class="selection_heading fr"/>-->
                                <h1 class="selection_heading fr resp_heading2">Have you been prescribed InVita D3 (cholecalciferol)</h1>
                                <div class="fl resp_switch2">
                                    <a href="_patient/patient.php" class="selection_link fr"></a>
                                    <span class="border blue fl"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="bottom_border fl"></span>
            </div>
            <div class="splash_footer h_mid">
                <img src="images/logo_footer.png" class="h_mid"/>
                <p class="cntr_txt fullwidth fl">
                    <b class="fullwidth fl">This website is sponsored by Consilient Health (UK) Limited.</b>
                    Copyright © 2014 Consilient Health. All rights reserved. Web agency Alchemy Interactive.
                    CH-VITD-063-10-2014 Date of Preparation July 2016
                </p>
            </div>
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/left_click1Hover.png",
                    "http://invita.alch.me/images/right_click1Hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            if ($(window).width() < 670) {
                $(".resp_heading").insertAfter(".resp_switch");
                $(".resp_heading2").insertAfter(".resp_switch2");
            }
        </script>
    </body>
</html>