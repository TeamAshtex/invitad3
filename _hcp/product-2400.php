<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Invita D3 - responsive project</title>

	<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />

	<!-- Vendor Styles -->

	<!-- App Styles -->
	<link rel="stylesheet" href="vendor/OwlCarousel2-develop/dist/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="css/style.css" />

	<!-- Vendor JS -->
	<script src="vendor/jquery-1.12.4.min.js"></script>
	<script src="vendor/OwlCarousel2-develop/dist/owl.carousel.min.js"></script>

	<!-- App JS -->
	<script src="js/functions.js"></script>
</head>

<body>
<div class="wrapper">
	<?php include 'includes/header.php';?>

	<div class="main">
		<div class="main__intro">
			<div class="shell">
				<?php include 'includes/nav.php';?>
	


			
				<h1 class="animated">Products</h1>

				<img class="products-intro animated" src="css/images/temp/products-intro.png" alt="">
			</div><!-- /.shell -->
		</div><!-- /.main__intro -->

		<div class="main__content">
			<div class="shell">
				<div class="product-info animated">
					<div class="product__aside">
						<img src="css/images/temp/product-info-3.png" alt="">

						<div class="cta cta--primary">
							<div class="cta__body animated">
								<ul>
									<li class="animated">
										<a href="#">How to take this?</a>
									</li>

									<li class="animated">
										<a href="#">Summary of Product Characteristics</a>
									</li>

									<li class="animated">
										<a href="#">Download packaging leaflet</a>
									</li>

									<li class="animated">
										<a href="#">Dosing and frequency</a>
									</li>
								</ul>
							</div><!-- /.cta__body -->
						</div><!-- /.cta -->
					</div><!-- /.product__aside -->

					<div class="product__content">
						<h3 class="text-green animated">InVita D3 2400 IU/ml</h3><!-- /.text-darkpink -->

						<div class="accordions animated">
							<div class="accordion animated">
								<div class="accordion__head animated">
									<h3>Product description</h3>
								</div><!-- /.accordion__head -->

								<div class="accordion__body animated">
									<p class="animated">InVita D3 2,400 IU/ml oral drops solution is licensed for use in pregnancy, breastfeeding and paediatrics aged 0-18 years.</p>

									<p class="animated">InVita D3 2,400 IU/ml oral drops, solution is a clear liquid with an orange odour. It is supplied in a 10 ml brown, molded glass bottles sealed with polypropylene screw cap. Each pack contains 1 brown, molded glass bottle containing 10 ml solution.</p>

									<ul class="list-info animated">
										<li class="animated">
											<strong>Dosage:</strong>
											2400 IU
										</li>
										
										<li class="animated">
											<strong>Product range:</strong>
											Oral solution
										</li>
										
										<li class="animated">
											<strong>Units per pack:</strong>
											1 x 10 ml ampoules
										</li>
									</ul><!-- /.list-info -->
								</div><!-- /.accordion__body -->
							</div><!-- /.accordion -->

							<div class="accordion animated">
								<div class="accordion__head animated">
									<h3>Licensed indications</h3>
								</div><!-- /.accordion__head -->
								
								<div class="accordion__body animated">
									<ul class="list-bullets list-bullets--gray animated">
										<li class="animated">Prevention and treatment of vitamin D deficiency in infants and children</li>
									</ul><!-- /.list-bullets -->
								</div><!-- /.accordion__body -->
							</div><!-- /.accordion -->
							
							<div class="accordion">
								<div class="accordion__head animated">
									<h3>Safety profile</h3>
								</div><!-- /.accordion__head -->
								
								<div class="accordion__body animated">
									<p class="animated">Pre-clinical studies conducted in various animal species have demonstrated that toxic effects occur in animals at doses much higher than those required for therapeutic use in humans.</p>

									<p class="animated">In toxicity studies at repeated doses, the effects most commonly reported were increased calciuria and decreased phosphaturia and proteinuria.</p>
									
									<p class="animated">Hypercalcaemia has been reported in high doses. In a state of prolonged hypercalcaemia, histological alterations (calcification) were more frequently borne by the kidneys, heart, aorta, testes, thymus and intestinal mucosa.</p>
									
									<p class="animated">Cholecalciferol has been shown to be teratogenic at high doses in animals.</p>
									
									<p class="animated">At doses equivalent to those used therapeutically, cholecalciferol has no teratogenic activity.</p>
									
									<p class="animated">Cholecalciferol has no potential mutagenic or carcinogenic activity.</p>

									<h6 class="animated">References:</h6>

									<ol class="list-references animated">
										<li class="animated">Cavalier E et al. Int Jour Endocrin 2013 <a href="#">http://dx.doi. org/10.1155/2013/327265</a>.</li>
										
										<li class="animated">Schleck M-L et al. Nutrients 2015, 5413-5422; <a href="#">doi:10.3390/nu7075227</a>.</li>
									</ol><!-- /.list-references -->
								</div><!-- /.accordion__body -->
							</div><!-- /.accordion -->
						</div><!-- /.accordions -->
					</div><!-- /.product__content -->
				</div><!-- /.product-info -->

				<div class="form-secondary animated">
					<form action="?" method="post">
						<div class="form__inner animated">
							<div class="form__head animated">
								<h1 class="text-orange animated">To hear about new and upcoming products enter your email opposite for email alerts.</h1><!-- /.text-orange -->
							</div><!-- /.form__head -->
							
							<div class="form__body animated">
								<div class="form__row animated">
									<label for="field-email" class="form__label">Email:</label>
									
									<div class="form__controls">
										<input type="text" class="form__field animated" name="field-email" id="field-email" value="" placeholder="">
									</div><!-- /.form__controls -->
								</div><!-- /.form__row -->

								<div class="form__actions animated">
									<input type="submit" value="Submit" class="form__btn animated">
								</div><!-- /.form__actions -->
							</div><!-- /.form__body -->
						</div><!-- /.form__inner -->
					</form>
				</div><!-- /.form -->
			</div><!-- /.shell -->
		</div><!-- /.main__content -->
	</div><!-- /.main -->

	<?php include 'includes/footer.php';?>
</div><!-- /.wrapper -->
</body>
</html>

