<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Invita D3 - responsive project</title>

	<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />

	<!-- Vendor Styles -->

	<!-- App Styles -->
	<link rel="stylesheet" href="vendor/OwlCarousel2-develop/dist/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="css/style.css" />

	<!-- Vendor JS -->
	<script src="vendor/jquery-1.12.4.min.js"></script>
	<script src="vendor/OwlCarousel2-develop/dist/owl.carousel.min.js"></script>

	<!-- App JS -->
	<script src="js/functions.js"></script>
</head>

<body>
<div class="wrapper">
	<?php include 'includes/header.php';?>

	<div class="main">
		<div class="main__intro">
			<div class="shell">
				<?php include 'includes/nav.php';?>

					<div class="slider animated">
						<div class="slides">
							<div class="slide">
								<div class="slide-image">
									<img src="css/images/temp/slide1.jpg" alt="">
								</div><!-- /.slide-image -->
							</div><!-- /.slide -->

							<div class="slide">
								<div class="slide-image">
									<img src="css/images/temp/slide2.jpg" alt="">
								</div><!-- /.slide-image -->
							</div><!-- /.slide -->

							<div class="slide">
								<div class="slide-image">
									<img src="css/images/temp/slide3.jpg" alt="">
								</div><!-- /.slide-image -->
							</div><!-- /.slide -->

							<div class="slide">
								<div class="slide-image">
									<img src="css/images/temp/slide4.jpg" alt="">
								</div><!-- /.slide-image -->
							</div><!-- /.slide -->
						</div><!-- /.slides -->
					</div><!-- /.slider -->
				</div><!-- /.shell -->
			</div><!-- /.main__intro -->

			<div class="main__content">
				<div class="shell">
					<div class="info-boxes animated">
						<div class="info">
							<h2>
								<a href="#">Treatment</a>
							</h2>

							<p>NOS guidelines recommend a treatment loading dose of 3000,000 IU for treatment of vitamin D deficiency.<sup>1</sup> Invita D# 50,000 IU is given as a weekly dose over 6 weeks for the treatment of Vitamin D deficiency and is suitable for adults. Invita D3 25,000 IU is suitable for treatment in children and is taken once every 2 weeks for 6 weeks.</p>

							<div class="info__link">
								<a href="#">Read more</a>
							</div><!-- /.info__link -->
						</div><!-- /.info -->

						<div class="info">
							<h2>
								<a href="#">Prevention</a>
							</h2>

							<p>Prevention of vitamin D3 is recommended in groups who may be at risk from vitamin D deficiency. These include:</p>

							<ul class="list-primary info__list">
								<li>Older patients (particularly in case of falls or fractures) or those who may be institutionalised on hospitalised</li>
								
								<li>Dark skinned individuals</li>
							</ul><!-- /.list-primary info__list -->

							<div class="info__link">
								<a href="#">Read more</a>
							</div><!-- /.info__link -->
						</div><!-- /.info -->
					</div><!-- /.info-boxes -->

					<div class="products animated">
						<div class="product product--darkpink">
							<div class="product__background">
								<span></span>
							</div><!-- /.product__background -->

							<a href="#">
								<img src="css/images/temp/product-darkpink.png" alt="">
							</a>

							<div class="product__content">
								<h4>
									<a href="#">New 50,000 IU/ml oral solution</a>
								</h4>

								<p>The simplicity of a UK guideline-aligned treatment course delivered in just 6 weekly doses<sup>1,3</sup></p>
							</div><!-- /.product__content -->
						</div><!-- /.product -->

						<div class="product product--blue">
							<div class="product__background">
								<span></span>
							</div><!-- /.product__background -->

							<a href="#">
								<img src="css/images/temp/product-blue.png" alt="">
							</a>
							
							<div class="product__content">
								<h4>
									<a href="#">25,000 IU/ml Oral solution</a>
								</h4>

								<p>A licensed vitamin D3 preparation suitable for patients of all ages<sup>2</sup></p>
							</div><!-- /.product__content -->
						</div><!-- /.product -->
						
						<div class="product product--green">
							<div class="product__background">
								<span></span>
							</div><!-- /.product__background -->

							<a href="#">
								<img src="css/images/temp/product-green.png" alt="">
							</a>
							
							<div class="product__content">
								<h4>
									<a href="#">2,400 Oral drops</a>
								</h4>

								<p>A licensed solution for use in pregnancy, breastfeeding and paediatrics aged 0-18 years<sup>1</sup></p>
							</div><!-- /.product__content -->
						</div><!-- /.product -->
						
						<div class="product product--pink">
							<div class="product__background">
								<span></span>
							</div><!-- /.product__background -->

							<a href="#">
								<img src="css/images/temp/product-pink.png" alt="">
							</a>
							
							<div class="product__content">
								<h4>
									<a href="#">800 IU soft gel capsules</a>
								</h4>

								<p>Are licensed for the  prophylaxis and treatment of vitamin D deficiency in adults and children over 12 years old.</p>
							</div><!-- /.product__content -->
						</div><!-- /.product -->
					</div><!-- /.products -->

					<div class="widget-weather animated">
						<div class="widget__head">
							<h3>Current weather where you are</h3>
						</div><!-- /.widget__head -->

						<div class="widget__body">
							<div class="widget__image">
								<i class="ico-sunny"></i>
							</div><!-- /.widget__image -->

							<div class="widget__content">
								<strong>21<sup>o</sup>c</strong>

								<span>3:06 pm BST</span>

								<span>Mostly Sunny</span>
							</div><!-- /.widget__content -->
						</div><!-- /.widget__body -->
					</div><!-- /.widget-weather -->
					
				</div><!-- /.shell -->
			</div><!-- /.main__content -->
		</div><!-- /.main -->

		<?php include 'includes/footer.php';?>
</div><!-- /.wrapper -->
</body>
</html>

