<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Invita D3 - responsive project</title>

	<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />

	<!-- Vendor Styles -->

	<!-- App Styles -->
	<link rel="stylesheet" href="vendor/OwlCarousel2-develop/dist/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="css/style.css" />

	<!-- Vendor JS -->
	<script src="vendor/jquery-1.12.4.min.js"></script>
	<script src="vendor/OwlCarousel2-develop/dist/owl.carousel.min.js"></script>

	<!-- App JS -->
	<script src="js/functions.js"></script>
</head>

<body>
<div class="wrapper">
	<?php include 'includes/header.php';?>

	<div class="main">
		<div class="main__intro">
			<div class="shell">
				<?php include 'includes/nav.php';?>
	

			
				<div class="intro animated">
					<div class="intro__image animated">
						<img src="css/images/temp/intro-how-to-take.png" alt="">
					</div><!-- /.intro__image -->
				
					<div class="intro__content animated">
						<h1>About InVita D3 - How to take InVita D3</h1>

						<div class="intro__video animated">
							<iframe src="https://www.youtube.com/embed/C0DPdy98e4c?ecver=1" frameborder="0" allowfullscreen></iframe>

							<!-- <video class="intro__video" autoplay="autoplay" preload="none" loop="" muted="" playsinline="" poster="css/images/temp/intro.jpg">
								<source src="resources/videos/video.mp4" type="video/mp4">
							</video> -->
						</div><!-- /.intro__video -->
					</div><!-- /.intro__content -->
				</div><!-- /.intro -->
			</div><!-- /.shell -->
		</div><!-- /.main__intro -->

		<div class="main__content">
			<div class="shell">
				<ul class="products-primary animated">
					<li class="product-primary animated">
						<div class="product__image animated">
							<img src="css/images/temp/product-primary1.png" alt="">
						</div><!-- /.product__image -->
						
						<div class="product__content">
							<h2 class="text-blue animated">
								InVita D3 25,000 and <span class="text-darkpink">50,000 IU/ml oral solution<sup>1,2</sup></span><!-- /.text-pink -->
							</h2>

							<p class="animated">InVita D3 oral solution is available in a single-dose ‘snap and squeeze’ ampoule presentation Patients should be advised to take InVita D3 at mealtimes</p>
						</div><!-- /.product__content -->
					</li><!-- /.product -->
					
					<li class="product-primary animated">
						<div class="product__image animated">
							<img src="css/images/temp/product-primary2.png" alt="">
						</div><!-- /.product__image -->
						
						<div class="product__content">
							<h2 class="text-green animated">InVita D3 2,400 IU/ml oral drops<sup>3</sup></h2><!-- /.text-green -->

							<ol class="list-tertiary product__list">
								<li class="animated">The product should be shaken before use</li>

								<li class="animated">Remove cap, tip the bottle up and the integrated dropper will steadily dispense drops</li>
								
								<li class="animated">The drops can be taken as is or mixed with a small amount of cold or lukewarm food immediately before use and you should make sure you take the entire dose</li>
								
								<li class="animated">For children InVita D3 can also be mixed with a small amount of children’s foods but should not be added to a bottle of milk or container of soft foods in case the child does not take the whole dose</li>
							</ol><!-- /.list-secondary -->
						</div><!-- /.product__content -->
					</li><!-- /.product -->
					
					<li class="product-primary animated">
						<div class="product__image animated">
							<img src="css/images/temp/product-primary3.png" alt="">
						</div><!-- /.product__image -->
						
						<div class="product__content animated">
							<h2 class="text-pink animated">InVita D3 800 IU soft gel capsules <span>4</span></h2><!-- /.text-pink -->

							<p class="animated">InvitaD3 800 IU soft gel capsules should be swallowed whole with water</p>
						</div><!-- /.product__content -->
					</li><!-- /.product -->
				</ul><!-- /.products-primary -->

				<h6 class="animated">References:</h6>

				<ol class="list-references animated">
					<li class="animated">Consilient Health Ltd. InVita D3 50,000 IU Oral Solution Summary of Product Characteristics.</li>
					
					<li class="animated">Consilient Health Ltd. InVita D3 25,000 IU Oral Solution Summary of Product Characteristics.</li>
					
					<li class="animated">Consilient Health Ltd. InVita D3 2,400IU oral drops Summary of Product Characteristics.</li>
					
					<li class="animated">Consilient Health Ltd. InVita D3 800IU soft gel capsules Summary of Product Characteristics.</li>
				</ol><!-- /.list-references -->

				<div class="demo-outer">
					<div class="demo animated">
						<div class="demo__image animated">
							<img src="css/images/temp/demo.jpg" alt="">
						</div><!-- /.demo__image -->

						<div class="demo__content animated">
							<p class="animated">'Squeeze' the full contents into your mouth. Then swallow.</p>

							<p class="animated">If preferred, the contents can be emptied onto a spoon and taken orally or mixed with a little cold or lukewarm foor</p>

							<a href="#" class="demo__btn animated">Replay Animation</a><!-- /.demo__btn -->
						</div><!-- /.demo__content -->
					</div><!-- /.demo -->
				</div><!-- /.demo-outer -->
			</div><!-- /.shell -->
		</div><!-- /.main__content -->
	</div><!-- /.main -->

	<?php include 'includes/footer.php';?>
</div><!-- /.wrapper -->
</body>
</html>

