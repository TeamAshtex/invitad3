<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Invita D3 - responsive project</title>

	<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />

	<!-- Vendor Styles -->

	<!-- App Styles -->
	<link rel="stylesheet" href="vendor/OwlCarousel2-develop/dist/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="css/style.css" />

	<!-- Vendor JS -->
	<script src="vendor/jquery-1.12.4.min.js"></script>
	<script src="vendor/OwlCarousel2-develop/dist/owl.carousel.min.js"></script>

	<!-- App JS -->
	<script src="js/functions.js"></script>
</head>

<body>
<div class="wrapper">
	<?php include 'includes/header.php';?>

	<div class="main">
		<div class="main__intro">
			<div class="shell">
				<?php include 'includes/nav.php';?>
	


				<h1 class="animated">National Osteoporosis Society guidelines<sup class="small">1</sup></h1>
				<br />

				
			</div><!-- /.shell -->
		</div><!-- /.main__intro -->

		<div class="main__content">
			<section class="section main__section section--gray-gradient animated" id="treatment">
				<div class="shell">
					<ul>
						<li>In 2013, the National Osteoporosis Society developed national clinical guidelines on the management of vitamin D deficiency in adult patients with, or at risk of developing, bone disease</li>
						<li>When treating vitamin D deficiency, oral vitamin D3 is recommended as the vitamin D preparation of choice and where rapid correction of vitamin D deficiency is required, the recommended treatment regimen is based&nbsp;on fixed loading doses followed by regular maintenance therapy</li>
						<li>Loading regimens for treatment of vitamin D deficiency up to a total of approximately 300,000 IU may be given either as weekly or daily split doses</li>
						<li>Maintenance regimens may be considered after loading, given either daily or intermittently at a higher equivalent dose</li>
					</ul>
					<h6>References:</h6>
					<ol class="list-references animated animated-in">
						<li>National Osteoporosis Society. Vitamin D and Bone Health: A Practical Clinical Guideline for Patient Management. April 2013.&nbsp;http://www.nos.org.uk/document.doc?id=1352. Accessed August 2015.</li>
					</ol>
				
					
				</div><!-- /.shell -->
			</section><!-- /.section -->
		</div><!-- /.main__content -->
	</div><!-- /.main -->

	<?php include 'includes/footer.php';?>
</div><!-- /.wrapper -->
</body>
</html>

