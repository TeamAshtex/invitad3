<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Invita D3 - responsive project</title>

	<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />

	<!-- Vendor Styles -->

	<!-- App Styles -->
	<link rel="stylesheet" href="vendor/OwlCarousel2-develop/dist/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="css/style.css" />

	<!-- Vendor JS -->
	<script src="vendor/jquery-1.12.4.min.js"></script>
	<script src="vendor/OwlCarousel2-develop/dist/owl.carousel.min.js"></script>

	<!-- App JS -->
	<script src="js/functions.js"></script>
</head>

<body>
<div class="wrapper">
	<?php include 'includes/header.php';?>

	<div class="main">
		<div class="main__intro">
			<div class="shell">
				<?php include 'includes/nav.php';?>
	


			
				<h1 class="animated">Products</h1>

				<img class="products-intro animated" src="css/images/temp/products-intro.png" alt="">
			</div><!-- /.shell -->
		</div><!-- /.main__intro -->

		<div class="main__content">
			<div class="shell">
				<div class="product-info">
					<div class="product__aside animated">
						<img src="css/images/temp/product-info-4.png" alt="">

						<div class="cta cta--primary animated">
							<div class="cta__body animated">
								<ul>
									<li class="animated">
										<a href="#">How to take this?</a>
									</li>

									<li class="animated">
										<a href="#">Summary of Product Characteristics</a>
									</li>

									<li class="animated">
										<a href="#">Download packaging leaflet</a>
									</li>

									<li class="animated">
										<a href="#">Dosing and frequency</a>
									</li>
								</ul>
							</div><!-- /.cta__body -->
						</div><!-- /.cta -->
					</div><!-- /.product__aside -->

					<div class="product__content">
						<h3 class="text-darkpink animated">InVita D3 800 IU soft gel capsules</h3><!-- /.text-darkpink -->

						<div class="accordions">
							<div class="accordion">
								<div class="accordion__head animated">
									<h3>Product description</h3>
								</div><!-- /.accordion__head -->

								<div class="accordion__body animated">
									<p class="animated">InVitaD3 800IU soft gel capsules are licensed for the prophylaxis and treatment of vitamin D deficiency in adults and children over 12 years old.</p>

									<ul class="list-info animated">
										<li class="animated">
											<strong>Dosage:</strong>
											8000 IU
										</li>
										
										<li class="animated">
											<strong>Product range:</strong>
											Soft gel capsules
										</li>
										
										<li class="animated">
											<strong>Units per pack:</strong>
											28 capsules packed in PVDC/Aluminium foil blisters, inserted into a cardboard
										</li>
									</ul><!-- /.list-info -->
								</div><!-- /.accordion__body -->
							</div><!-- /.accordion -->

							<div class="accordion">
								<div class="accordion__head animated">
									<h3>Licensed indications</h3>
								</div><!-- /.accordion__head -->
								
								<div class="accordion__body animated">
									<ul class="list-bullets list-bullets--gray animated">
										<li class="animated">Prophylaxis and treatment of vitamin D deficiency in adults and children over 12 years old</li>
										
										<li class="animated">Adjunct to specific therapy for osteoporosis in patients with vitamin D deficiency, or at risk of vitamin D insufficiency</li>
									</ul><!-- /.list-bullets -->
								</div><!-- /.accordion__body -->
							</div><!-- /.accordion -->
							
							<div class="accordion">
								<div class="accordion__head animated">
									<h3>Safety profile</h3>
								</div><!-- /.accordion__head -->
								
								<div class="accordion__body animated">
									<p class="animated">No variation in calcium levels or clinically relevant adverse events noted in 2 studies with 25,000IU/ml or 50,000 IU/ml dosage 1,2</p>

									<p class="animated">In a study with four different therapeutic dosing schedules using multiples of 25,000 IU, with daily intake as high as 4167 IU per day in one group, no clinically significant change in plasma calcium or phosphorus concentrations were observed<sup>1</sup></p>

									<p class="animated">A dose-response study using total treatment doses of vitamin D up to 400,000 IU across the 8-week study period reported no vitamin D or calcium related adverse events<sup>2</sup></p>

									<h6 class="animated">References:</h6>

									<ol class="list-references animated">
										<li class="animated">Cavalier E et al. Int Jour Endocrin 2013 <a href="#">http://dx.doi. org/10.1155/2013/327265</a>.</li>
										
										<li class="animated">Schleck M-L et al. Nutrients 2015, 5413-5422; <a href="#">doi:10.3390/nu7075227</a>.</li>
									</ol><!-- /.list-references -->
								</div><!-- /.accordion__body -->
							</div><!-- /.accordion -->
						</div><!-- /.accordions -->
					</div><!-- /.product__content -->
				</div><!-- /.product-info -->

				<div class="form-secondary animated">
					<form action="?" method="post">
						<div class="form__inner animated">
							<div class="form__head animated">
								<h1 class="text-orange">To hear about new and upcoming products enter your email opposite for email alerts.</h1><!-- /.text-orange -->
							</div><!-- /.form__head -->
							
							<div class="form__body animated">
								<div class="form__row">
									<label for="field-email" class="form__label">Email:</label>
									
									<div class="form__controls">
										<input type="text" class="form__field animated" name="field-email" id="field-email" value="" placeholder="">
									</div><!-- /.form__controls -->
								</div><!-- /.form__row -->

								<div class="form__actions">
									<input type="submit" value="Submit" class="form__btn animated">
								</div><!-- /.form__actions -->
							</div><!-- /.form__body -->
						</div><!-- /.form__inner -->
					</form>
				</div><!-- /.form -->
			</div><!-- /.shell -->
		</div><!-- /.main__content -->
	</div><!-- /.main -->

	<?php include 'includes/footer.php';?>
</div><!-- /.wrapper -->
</body>
</html>

