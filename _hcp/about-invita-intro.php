<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Invita D3 - responsive project</title>

	<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />

	<!-- Vendor Styles -->

	<!-- App Styles -->
	<link rel="stylesheet" href="vendor/OwlCarousel2-develop/dist/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="css/style.css" />

	<!-- Vendor JS -->
	<script src="vendor/jquery-1.12.4.min.js"></script>
	<script src="vendor/OwlCarousel2-develop/dist/owl.carousel.min.js"></script>

	<!-- App JS -->
	<script src="js/functions.js"></script>
</head>

<body>
<div class="wrapper">
	<?php include 'includes/header.php';?>

	<div class="main">
		<div class="main__intro">
			<div class="shell">
				<?php include 'includes/nav.php';?>
	


				<div class="intro-primary animated">
					<h1>About InVita D3 range</h1>

					<img src="css/images/temp/intro-primary.png" alt="">

					<div class="intro__content">
						<p>
							Vitamin D deficiency.
							<br/>We've got it covered
						</p>

						<p>UK licensed for vitamin D deficiency from <strong><a href="#" class="text-darkpink">treatment</a></strong> to <strong><a href="#" class="text-blue">prevention</a></strong><sup>1,2</sup></p>
					</div><!-- /.intro__content -->
				</div><!-- /.intro-primary -->
			</div><!-- /.shell -->
		</div><!-- /.main__intro -->

		<div class="main__content main__content--primary">
			<div class="shell">
				<div class="slider-primary animated">
					<div class="slides">
						<div class="slide slide--pink">
							<div class="slide__inner">
								<div class="slide__image">
									<img src="css/images/temp/product-secondary1.png" alt="">
								</div><!-- /.slide__image -->

								<div class="slide__content">
									<h2><strong>NEW</strong> 800 IU soft gel capsules</h2>

									<p>800IU sofl gel capsulet are licensed for the prophylaxis and treatment of vitamin D deficiency in adults and children over 12 years old.<sup>1</sup>
									</p>

									<p>a convenient daily dose for £2.50 a month.<sup>1</sup></p>

								</div><!-- /.slide__content -->
							</div><!-- /.slide__inner -->
							
							<h6>References:</h6>
							<ol class="list-references">
								<li>Consilient Health Ltd. InVita D3 800 IU soft gel capsules Summary of Product Characteristics.</li>
							</ol><!-- /.list-references -->
						</div><!-- /.slide slide-800 -->

						<div class="slide slide--green">
							<div class="slide__inner">
								<div class="slide__image">
									<img src="css/images/temp/product-secondary2.png" alt="">
								</div><!-- /.slide__image -->

								<div class="slide__content">
									<h2>2,400 IU drops</h2>

									<p>2,400 IU drops solution is licensed for use in pregnancy, breastfeeding and paediatrics aged 0-18 years.<sup>1</sup></p>

									<p>Oral vitamin D3 drops that deliver doses consistent with UK guidelines for just £1.80 ot £2.70 a month (30 calendat days).<sup>1,2</sup></p>

								</div><!-- /.slide__content -->
							</div><!-- /.slide__inner -->
							
							<h6>References:</h6>

							<ol class="list-references">
								<li>Consilient Health Ltd. InVita D3 2,400 IU Oral Drops Summary of Product Characteristics.</li>

								<li>National Osteoporosis Society. Vitamin D and Bone Health: A Practical Clinical Guideline for Patient Management April 2013;v1.</li>
							</ol><!-- /.list-references -->
						</div><!-- /.slide slide-24 -->

						<div class="slide slide--blue">
							<div class="slide__inner">
								<div class="slide__image">
									<img src="css/images/temp/product-secondary3.png" alt="">
								</div><!-- /.slide__image -->	

								<div class="slide__content">
									<h2>25,000 IU/ml Oral solution</h2>

									<p>A licensed vitamin D3 preparation suitable fot patients of all ages - paediatrics ages for 0-18 years, adilts and elderly.<sup>1</sup></p>

									<p>Provides a regimen that delivers doses of vitamin D3 that are consistent with current NOS guidelines<sup>2</sup> without the burden on daily pill taking.</p>

									<p>The convenience of monthly adult prevention dose fot only £1.48 a month.<sup>1</sup></p>

								</div><!-- /.slide__content -->
							</div><!-- /.slide__inner -->
								
							<h6>References:</h6>

							<ol class="list-references">
								<li>Consilient Health Ltd. InVita D3 25,000 IU Oral Solution Summary of Product Characteristics.</li>
								
								<li>National Osteoporosis Society. Vitamin D and Bone Health: A Practical Clinical Guideline for Patient Management April 2013;v1.</li>
							</ol><!-- /.list-references -->
						</div><!-- /.slide slide-25 -->

						<div class="slide slide--darkpink">
							<div class="slide__inner">
								<div class="slide__image">
									<img src="css/images/temp/product-secondary4.png" alt="">
								</div><!-- /.slide__image -->

								<div class="slide__content">
									<h2>50,000 IU/ml oral solution</h2>

									<p>The simplicity of a UK guideline-aligned treatment course delivered in just 6 weekly doses<sup>2</sup></p>

								</div><!-- /.slide__content -->
							</div><!-- /.slide__inner -->

							<h6>References:</h6>

							<ol class="list-references">
								<li>Consilient Health Ltd. InVita D3 50,000 IU Oral Solution Summary of Product Characteristics.</li>
								
								<li>National Osteoporosis Society. Vitamin D and Bone Health: A Practical Clinical Guideline for Patient Management April 2013;v1.</li>
							</ol><!-- /.list-secondary -->
						</div><!-- /.slide -->
					</div><!-- /.slides -->
				</div><!-- /.slider -->

				<div class="cta animated">
					<div class="cta__head">
						<p>Explore further...</p>
					</div><!-- /.cta__head -->

					<div class="cta__body">
						<ul>
							<li>
								<a href="#">Who can take it?</a>
							</li>
						
							<li>
								<a href="#">How to take InVita D3</a>
							</li>
						
							<li>
								<a href="#">Dosing frequency</a>
							</li>
						</ul>
					</div><!-- /.cta__body -->
				</div><!-- /.cta -->
			</div><!-- /.shell -->
		</div><!-- /.main__content -->
	</div><!-- /.main -->

	<?php include 'includes/footer.php';?>
</div><!-- /.wrapper -->
</body>
</html>

