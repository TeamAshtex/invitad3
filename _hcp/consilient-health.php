<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Invita D3 - responsive project</title>

	<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />

	<!-- Vendor Styles -->

	<!-- App Styles -->
	<link rel="stylesheet" href="vendor/OwlCarousel2-develop/dist/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="css/style.css" />

	<!-- Vendor JS -->
	<script src="vendor/jquery-1.12.4.min.js"></script>
	<script src="vendor/OwlCarousel2-develop/dist/owl.carousel.min.js"></script>

	<!-- App JS -->
	<script src="js/functions.js"></script>
</head>

<body>
<div class="wrapper">
	<?php include 'includes/header.php';?>

	<div class="main">
		<div class="main__intro">
			<div class="shell">
				<?php include 'includes/nav.php';?>
	


				<h1 class="animated">Consilient Health</h1>
				<br />

				
			</div><!-- /.shell -->
		</div><!-- /.main__intro -->

		<div class="main__content">
			<section class="section main__section section--gray-gradient animated" id="treatment">
				<div class="shell">
					<ul>
						<li>Consilient Health have over 10 years’ experience in the UK</li>
						<li>We are passionate about developing and supplying prescription medicines that provide benefits to both patients and healthcare professionals</li>
						<li>InVita D3 is manufactured within Europe<sup>1</sup></li>
					</ul>
					
					<h6>Reference:</h6>
					<ol class="list-references animated animated-in">
						<li>Data on file: DOF008. Consilient Health Ltd.</li>
					</ol>
				
					
				</div><!-- /.shell -->
			</section><!-- /.section -->
		</div><!-- /.main__content -->
	</div><!-- /.main -->

	<?php include 'includes/footer.php';?>
</div><!-- /.wrapper -->
</body>
</html>

