				<nav class="nav">
					<a href="#" class="burger-btn active nav__btn">
						<span></span>
						<span></span>
						<span></span>
					</a>

					<ul>
						<li>
							<a href="#">About InVita D3</a>

							<ul class="nav__dropdown">
								<li>
									<a href="about-invita-intro.php">Intro</a>
								</li>

								<li>
									<a href="who-can-take-it.php">Who can take it?</a>
								</li>
								
								<li>
									<a href="how-to-take-invita-d3.php">How to take InVita D3</a>
								</li>
								
								<li>
									<a href="dosing-and-frequency.php">Dosing &amp; frequency</a>
								</li>
								
								<li>
									<a href="medicine-v-suplement.php">Medicine vs supplemeny</a>
								</li>
							</ul><!-- /.nav__dropdown -->

							<a href="#" class="nav__arrow">
								<span></span>
								<span></span>
								<span></span>
							</a>
						</li>

						<li>
							<a href="products-intro.php">Products</a>

							<ul class="nav__dropdown">
								<li>
									<a href="product-5000.php">InVita D3 50,000 IU/ml</a>
								</li>
								
								<li>
									<a href="product-25000.php">InVita D3 25,000 IU/ml</a>
								</li>
								
								<li>
									<a href="product-2400.php">InVita D3 2,400 IU</a>
								</li>
								
								<li>
									<a href="product-800.php">InVita D3 800 IU</a>
								</li>
							</ul><!-- /.nav__dropdown -->

							<a href="#" class="nav__arrow">
								<span></span>
								<span></span>
								<span></span>
							</a>
						</li>

						<li>
							<a href="consilient-health.php">Consilient Health</a>

							<a href="#" class="nav__arrow">
								<span></span>
								<span></span>
								<span></span>
							</a>
						</li>

						<li>
							<a href="nos-guidelines.php">NOS Guidelines</a>

							<a href="#" class="nav__arrow">
								<span></span>
								<span></span>
								<span></span>
							</a>
						</li>

						<li>
							<a href="prescribing-information.php">Prescribing Information</a>

							<a href="#" class="nav__arrow">
								<span></span>
								<span></span>
								<span></span>
							</a>
						</li>
					</ul>
				</nav><!-- /.nav -->