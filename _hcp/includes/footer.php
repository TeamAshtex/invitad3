	<footer class="footer">		
		<div class="footer__inner">
			<div class="footer__bar"></div><!-- /.footer__bar -->
			<div class="footer__bar"></div><!-- /.footer__bar -->

			<div class="shell">
				<div class="footer__aside">
					<div class="widget-primary">
						<div class="widget__image">
							<i class="ico-warning"></i>
						</div><!-- /.widget__image -->

						<div class="widget__content">
							<h6>Reporting of side effects</h6>

							<p>Adverse events should be reported. Reporting forms and information can be found at <a href="#">https://yellowcard.mhra.gov.uk/</a>. Adverse events should also be reported to Consilient Health (UK) Ltd, Ground Floor, No. 1 Church Road, Richmond upon Thames, Surrey TW9 2QE UK or drugsafety@consilienthealth.com.</p>
						</div><!-- /.widget__content -->
					</div><!-- /.widget -->
				</div><!-- /.footer__aside -->

				<div class="footer__content">
					<nav class="nav-secondary footer__nav">
						<ul>
							<li>
								<a href="#">About InVita D3</a>
							</li>

							<li>
								<a href="#">How to take InVita D3</a>
							</li>
							
							<li>
								<a href="#">Acquisition Costs</a>
							</li>
							
							<li>
								<a href="#">Consilient Health</a>
							</li>
							
							<li>
								<a href="#">NOS Guidelines</a>
							</li>
							
							<li>
								<a href="#">Prescribing Information</a>
							</li>
						</ul>
					</nav><!-- /.nav -->

					<nav class="nav-secondary footer__nav">
						<ul>
							<li>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Contact Us</a>
							</li>
							
							<li>
								<a href="#">Sitemap</a>
							</li>
							
							<li>
								<a href="#">Terms and Conditions</a>
							</li>
							
							<li>
								<a href="#">Cookies</a>
							</li>
						</ul>
					</nav><!-- /.nav -->
				</div><!-- /.footer__content -->
			</div><!-- /.shell -->
		</div><!-- /.footer__inner -->

		<div class="copyright">
			<div class="shell">
				<p>
					<a href="#" class="logo-consilient-health">Consilient Health</a>
				</p>

				<strong>This website is sponsored by Consilient Health (UK) Limited.</strong>

				<p>Copyright &copy; 2014 Consilient Health. All rights reserved. Web agency Alchemy Interactive.<br/>CH-VITD-063-10-2014 Date of Preparation July 2016</p>
			</div><!-- /.shell -->
		</div><!-- /.copyright -->
	</footer><!-- /.footer -->