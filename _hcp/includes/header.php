	<header class="header">
		<div class="shell">
			<a href="#" class="logo">
				<i class="logo-icon">Invita 33 Colecalciferol</i>
				
				<span>Vitamin D deficiency, we've got it covered</span>
			</a><!-- /.logo -->

			<a href="#" class="burger-btn">
				<span></span>
				<span></span>
				<span></span>
			</a>

			<div class="header__aside">
				<nav class="nav-primary">
					<ul>
						<li>
							<a href="#">Professionals</a>
						</li>

						<li>
							<a href="#">Patients</a>
						</li>

						<li>
							<a href="#">Contact</a>
						</li>
					</ul>
				</nav><!-- /.nav-primary -->
			</div><!-- /.header__aside -->

			<div class="form-subscribe header__form">
				<form action="?" method="post">
					<div class="form__row">
						<label for="field-email" class="form__label">Enter email to recieve updates</label>
						
						<div class="form__controls">
							<input type="text" class="form__field" name="field-email" id="field-email" value="" placeholder="Enter email to recieve updates">
						</div><!-- /.form__controls -->

						<div class="form__actions">
							<input type="submit" value="Submit" class="form__btn">
						</div><!-- /.form__actions -->
					</div><!-- /.form__row -->
				</form>
			</div><!-- /.form-subscribe -->
		</div><!-- /.shell -->
	</header><!-- /.header -->