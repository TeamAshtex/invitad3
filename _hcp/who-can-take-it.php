<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Invita D3 - responsive project</title>

	<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />

	<!-- Vendor Styles -->

	<!-- App Styles -->
	<link rel="stylesheet" href="vendor/OwlCarousel2-develop/dist/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="css/style.css" />

	<!-- Vendor JS -->
	<script src="vendor/jquery-1.12.4.min.js"></script>
	<script src="vendor/OwlCarousel2-develop/dist/owl.carousel.min.js"></script>

	<!-- App JS -->
	<script src="js/functions.js"></script>
</head>

<body>
<div class="wrapper">
	<?php include 'includes/header.php';?>

	<div class="main">
		<div class="main__intro">
			<div class="shell">
				<?php include 'includes/nav.php';?>
	

	
				<h1 class="animated">About InVita D3 - Who can take it?</h1>

				<div class="articles-placeholder"></div><!-- /.articles-placeholder -->
			</div><!-- /.shell -->
		</div><!-- /.main__intro -->

		<div class="main__content">
			<div class="shell">
				<div class="articles">
					<div class="article">
						<div class="article__image animated" style="background-image: url(css/images/temp/article1.png);">
							<div class="article__head animated">
								<h3 class="text-darkpink">Adults and Elderly</h3>
							</div><!-- /.article__head -->
						</div><!-- /.article__image -->

						<div class="article__content">
							<p class="text-darkpink animated">There are a number of adults who are more at risk of becoming deficient in vitamin D than others. These include:<sup>1</sup></p><!-- /.text-darkpink -->

							<ul class="list-secondary list--darkpink text-darkpink article__list animated">
								<li class="animated">Older people, aged 65 years and over</li>
								
								<li class="animated">People who have low or no exposure to the sun, for example those who cover their skin for cultural reasons, who are housebound or who are confined indoors for long periods</li>
								
								<li class="animated">People who have darker skin, for example people of African, African-Caribbean or South Asian origin, because their bodies are not able to make as much vitamin D</li>
							</ul><!-- /.list-bullets -->

							<h4 class="text-darkpink animated">Why Invita D3?</h4>

							<p class="animated">The Invita D3 range is suitable for adults and the elderly as either as treatment or prevention. The range offers a daily, weekly or monthly option for patients. Invita D3 50,000 IU oral solution is given as 6 weekly doses for treatment and Invita D3 25,000 IU is given as a monthly dose for prevention. Invita D3 800IU can be given as a daily dose for either treatment or prevention. The oral solutions are gelatin free and suitable for vegetarians. Both the 50,000 IU and the 25,000 IU come as single-dose ‘snap and squeeze’ ampoule presentations.</p>

							<small class="animated">1. Consilient Health Ltd. InVita D3 50,000 IU/ml oral drops, solution. Summary of Product Characteristics.</small>
						</div><!-- /.article__content -->
					</div><!-- /.article -->

					<div class="article">
						<div class="article__image animated" style="background-image: url(css/images/temp/article2.png);">
							<div class="article__head animated">
								<h3 class="text-blue">Children</h3>
							</div><!-- /.article__head -->
						</div><!-- /.article__image -->

						<div class="article__content">
							<p class="text-blue animated">The Department of Health recommend that all infants and children under 5 years should take daily vitamin D in the form of drops<sup>2</sup> (unless infants are receiving infant formula as this is fortified with vitamin D). Vitamin D deficiency impairs the absorption of dietary calcium and phosphorus, which can give rise to bone problems such as rickets in children, and bone pain and tenderness as a result of osteomalacia in adults.</p><!-- /.text-darkpink -->

							<h4 class="text-blue animated">Why Invita D3?</h4>

							<p class="animated">The InvitaD3 range is suitable for children as either treatment or prevention. Invita D3 25,000IU oral solution can be given for treatment from 0-18 years as 1 oral solution every 2 weeks for 6 weeks. Invita D3 2,400 drops are indicated for the prevention of vitamin D deficiency in infants and children. InvitaD3 2,400 IU drops are gelatin free and suitable for vegetarians. InvitaD3 800IU soft capsules can be given to children over 12 years old as a daily dose for either treatment or prevention.</p>

							<small class="animated">2. Royal College of Paediatrics and Child Health. Guide for Vitamin D in Childhood. October 2013.</small>
						</div><!-- /.article__content -->
					</div><!-- /.article -->

					<div class="article">
						<div class="article__image animated" style="background-image: url(css/images/temp/article3.png);">
							<div class="article__head animated">
								<h3 class="text-green">Pregnant Women</h3>
							</div><!-- /.article__head -->
						</div><!-- /.article__image -->

						<div class="article__content">
							<p class="text-green animated">The Department of Health recommend that all pregnant and breastfeeding women should take a daily supplement containing 10µg of vitamin D (equivalent to 400 IU), to ensure the mother’s requirements for vitamin D are met and to build adequate fetal stores for early infancy.<sup>3</sup></p><!-- /.text-darkpink -->

							<h4 class="text-green animated">Why Invita D3?</h4>

							<p class="animated">Invita D3 2,400 IU drops are indicated for the prevention of Vitamin D deficiency in pregnancy and breast feeding. The dose of Invita D3 2,400 IU is 6 drops daily.</p>

							<small class="animated">
								1. Letter from the Chief Medical Officers for the United Kingdom. [accessed 29 06 12]
								
								<a href="#">http://www.dh.gov.uk/en/ Publicationsandstatistics/ Lettersandcirculars/ Dearcolleagueletters/DH_132509</a>
							</small>
						</div><!-- /.article__content -->
					</div><!-- /.article -->
				</div><!-- /.articles -->
			</div><!-- /.shell -->
		</div><!-- /.main__content -->
	</div><!-- /.main -->

	<?php include 'includes/footer.php';?>
</div><!-- /.wrapper -->
</body>
</html>

