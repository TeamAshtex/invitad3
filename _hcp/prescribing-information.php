<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Invita D3 - responsive project</title>

	<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />

	<!-- Vendor Styles -->

	<!-- App Styles -->
	<link rel="stylesheet" href="vendor/OwlCarousel2-develop/dist/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="css/style.css" />

	<!-- Vendor JS -->
	<script src="vendor/jquery-1.12.4.min.js"></script>
	<script src="vendor/OwlCarousel2-develop/dist/owl.carousel.min.js"></script>

	<!-- App JS -->
	<script src="js/functions.js"></script>
</head>

<body>
<div class="wrapper">
	<?php include 'includes/header.php';?>

	<div class="main">
		<div class="main__intro">
			<div class="shell">
				<?php include 'includes/nav.php';?>
	


				<h1 class="animated">Prescribing Information</h1>
				<br />

				
			</div><!-- /.shell -->
		</div><!-- /.main__intro -->

		<div class="main__content">
			<section class="section main__section section--gray-gradient animated" id="treatment">
				<div class="shell">
					<p style="text-align: justify; padding-right: 0%;"><strong>Abbreviated Prescribing Information - for full prescribing information, including side effects, precautions and contra-indications, see Summary of Product Characteristics (SmPC)</strong><br /><br /><strong>Product name and Composition: InVita D3 25,000 IU oral solution</strong>: 1 ml solution (1 ampoule) contains 0.625 mg cholecalciferol, equivalent to 25,000 IU vitamin D. <strong>InVita D3 2,400 IU/ml oral drops, solution</strong>: 1 ml solution (36 drops) contains 0.06 mg cholecalciferol, equivalent to 2,400 IU vitamin D3. <strong>Indications: InVita D3 25,000 IU oral solution</strong>: The prevention and treatment of vitamin D deficiency. As an adjunct to specific therapy for osteoporosis in patients with vitamin D deficiency or at risk of vitamin D insufficiency. <strong>InVita D3 2,400 IU/ml oral drops, solution</strong>: Prevention of vitamin D deficiency in infants and children. Prevention of vitamin D deficiency in pregnant and breast-feeding women. <strong>Dosage and administration: InVita D3 25,000 IU oral solution</strong>: <span style="text-decoration: underline;">Infants and children</span>: Prevention of deficiency, 0-1 year 25000 IU (1 ampoule) every 8 weeks; 1-18 years 25000 IU (1 ampoule) every 6 weeks. Treatment of deficiency, 0-18 years 25000 IU (1 ampoule) once every 2 weeks for 6 weeks followed by maintenance therapy of 400-1000 IU/day. <span style="text-decoration: underline;">Pregnancy and breastfeeding</span>: not recommended. <span style="text-decoration: underline;">Adults</span>: Prevention of deficiency, 25000 IU (1 ampoule) per month; higher doses and monitoring of serum 25(OH)D may be required in populations at high risk of vitamin D deficiency (including those who are institutionalised or hospitalised, dark skinned, obese, being evaluated for osteoporosis, with limited effective sun exposure due to protective clothing or consistent use of sun screens, using certain concomitant medication e.g. anticonvulsants or glucocorticoids, with malabsorption, including inflammatory bowel disease and coeliac disease and recently treated for vitamin D deficiency, and requiring maintenance therapy). Adjunct to specific therapy for osteoporosis, 25000 IU (1 ampoule) per month. Treatment of vitamin D deficiency (&lt;25 ng/ml), 50000 IU/week (2 ampoules) for 6-8 weeks, followed by maintenance therapy (1400-2000 IU/day may be required; check 25(OH)D measurements 3-4 months after starting maintenance therapy). ). <span style="text-decoration: underline;">Renal impairment</span>: InVita D3 should not be used in combination with calcium in patients with severe renal impairment. <strong>InVita D3 2,400 IU/ml oral drops, solution</strong>: <span style="text-decoration: underline;">Infants and Children</span>: Age 0-1 years 400 IU/day (6 drops); Age 1-18 years 600 IU/day (9 drops). <span style="text-decoration: underline;">Pregnancy and breastfeeding</span>: 400 IU/day (6 drops). <span style="text-decoration: underline;">Special populations</span> renal impairment: no specific adjustment required. Obese patients, patients with malabsorption syndromes, and patients on medications affecting vitamin D metabolism: higher doses are required for the treatment and prevention of vitamin D deficiency (2 - 3 times higher). Take InVita D3 orally, preferably with food, either directly or by mixing with a small amount of cold or lukewarm food immediately prior to use. Ensure that the entire dose is taken. For children who are not breast-feeding, the prescribed dose should be administered with a meal. <strong>Contraindications: InVita D3 25,000 IU oral solution</strong>: Hypersensitivity to the active substance or to any of the excipients; hypercalcaemia and/or hypercalciuria; nephrolithiasis and/or nephrocalcinosis; serious renal impairment; hypervitaminosis D; pseudohypoparathyroidism. <strong>InVita D3 2,400 IU/ml oral drops, solution</strong>: Hypersensitivity to the active substance or to any of the excipients; hypercalcaemia and/or hypercalciuria; hypervitaminosis D; kidney stones (nephrolithiasis, nephrocalcinosis) in patients with current chronic hypercalcaemia. <strong>Warnings and precautions</strong>: Use with caution in impaired renal function; monitor effect on calcium and phosphate levels. Consider the risk of soft tissue calcification. Exercise caution in patients receiving treatment for cardiovascular disease as concomitant administration of vitamin D with drugs containing digitalis and other cardiac glycosides may increase risk of digitalis toxicity and arrhythmia; strict medical supervision is needed, with serum calcium concentration and electrocardiographic monitoring if necessary. Use with caution in patients with sarcoidosis due to possible increase in vitamin D metabolism; monitor serum and urinary calcium levels in these patients. Allow for the total dose of vitamin D where patients consume treatments and / or foodstuffs enriched with vitamin D and for the patient’s level of sun exposure. Possible risk of renal stones, especially with concomitant calcium supplementation; consider the need for additional calcium supplementation for individual patients. Calcium supplements should be given under close medical supervision. <strong>Undesirable effects</strong>: Uncommon (&gt;1/1,000, &lt;1/100): Hypercalcaemia and hypercalciuria. Rare (&gt;1/10,000, &lt;1/1,000): pruritus, rash, urticaria. <strong>NHS Price: InVita D3 25,000 IU oral solution</strong>: £4.45 per pack of 3 x 1ml ampoules <strong>InVita D3 2,400 IU/ml oral drops, solution</strong>: £3.60 per 10ml bottle. <strong>Legal Classification</strong>: POM. <strong>MA number: InVita D3 25,000 IU oral solution</strong>: PL 24837/0039 <strong>InVita D3 2,400 IU/ml oral drops, solution</strong>: PL 24837/0046 <strong>Marketing Authorisation Holder</strong>: Consilient Health Limited, 5th Floor, Beaux Lane House, Mercer Street Lower, Dublin 2, Ireland. Further information is available on request from Consilient Health (UK) Ltd, 1 Church Road, Richmond upon Thames, Surrey, TW9 2QE or <a href="mailto:drugsafety@consilienthealth.com">drugsafety@consilienthealth.com</a>.
					</p>
					
					<table style="width: 100%;">
						<tbody>
							<tr>
								<td><strong>Date of preparation of PI: </strong>March 2015&nbsp;</td>
								<td style="text-align: right;"><span style="text-align: right;">CH-VITD-042-03-2015</span><strong>&nbsp;</strong>
								</td>
							</tr>
						</tbody>
					</table>
					
					<div style="text-align: center; border: black 2px solid; padding: 10px;"><strong>Adverse events should be reported. Reporting forms and information can be found at</strong><br /><br /><strong><a href="http://www.mhra.gov.uk/yellowcard" class="externallink">www.mhra.gov.uk/yellowcard</a>.</strong><br /><br /><strong>Adverse events should also be reported to Consilient Health (UK) Ltd, Ground Floor, No. 1 Church Road, Richmond upon Thames, Surrey TW9 2QE UK or <br /><a href="mailto:drugsafety@consilienthealth.com">drugsafety@consilienthealth.com</a></strong>
					</div>


				
					
				</div><!-- /.shell -->
			</section><!-- /.section -->
		</div><!-- /.main__content -->
	</div><!-- /.main -->

	<?php include 'includes/footer.php';?>
</div><!-- /.wrapper -->
</body>
</html>

