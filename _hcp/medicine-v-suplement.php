<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Invita D3 - responsive project</title>

	<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />

	<!-- Vendor Styles -->

	<!-- App Styles -->
	<link rel="stylesheet" href="vendor/OwlCarousel2-develop/dist/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="css/style.css" />

	<!-- Vendor JS -->
	<script src="vendor/jquery-1.12.4.min.js"></script>
	<script src="vendor/OwlCarousel2-develop/dist/owl.carousel.min.js"></script>

	<!-- App JS -->
	<script src="js/functions.js"></script>
</head>

<body>
<div class="wrapper">
	<?php include 'includes/header.php';?>

	<div class="main">
		<div class="main__intro">
			<div class="shell">
				<?php include 'includes/nav.php';?>
	


				<h1 class="main__heading animated">About InVita D3 - Medicine v supplement</h1>
			</div><!-- /.shell -->
		</div><!-- /.main__intro -->

		<div class="main__content">
			<div class="shell">
				<div class="compare animated">
					<img src="css/images/temp/medicine-v-suplement.png" class="animated" alt="">

					<div class="compare__inner">						
						<div class="compare__content animated">
							<h2 class="text-orange animated">What is a medicine?</h2>

							<p class="animated">
								<strong>A medicinal product is:1 any substance or combination of substances presented as having properties of preventing or treating disease in human beings; or any substance or combination of substances that may be used by or administered to human beings with a view to:</strong>
							</p>

							<ul class="list-bullets compare__list animated">
								<li class="animated">Restoring, correcting or modifying a physiological function by exerting a pharmacological, immunological or metabolic action, or</li>
								
								<li class="animated">Making a medical diagnosis</li>
							</ul><!-- /.list-bullets -->

							<p class="animated">
								<strong>Medicines and good manufacturing practice (gmp) <sup>10</sup></strong>
							
								Medicines are manufactured to GMP standards at premises inspected by local regulatory authorities GMP is that part of quality assurance that ensures medicines are consistently  produced and controlled to the quality standards appropriate to their intended use and as required by their marketing authorisation. Without GMP it is impossible to be sure that every unit of a medicine is of the same quality medicines are manufactured within tightly-defined product specification requirements.
							</p>

							<ul class="list-bullets compare__list animated">
								<li class="animated">For InVita D3 (cholecalciferol) 25,000 IU oral liquid, the cholecalciferol content must be between 95-105% of the stated value <sup>11</sup></li>
							</ul><!-- /.list-bullets -->
						</div><!-- /.compare__content -->
						
						<div class="compare__content animated">
							<h2 class="text-blue animated">Supplements are: <sup>2</sup></h2>

							<p class="animated">
								<strong>“foodstuffs the purpose of which is to supplement the normal diet and which are concentrated sources of nutrients or other substances with a nutritional or physiological effect, alone or in combination, marketed in dose form”</strong>
							</p>

							<ul class="list-bullets compare__list animated">
								<li class="animated">Food supplements are not intended to prevent or treat any disease<sup>6</sup></li>
								
								<li class="animated">Medicines can be distinguished from food supplements by their marketing authorisation<sup>1</sup></li>
								
								<li class="animated">There is no requirement for food supplements to be registered or authorised for sale in the UK<sup>3</sup></li>
							</ul><!-- /.list-bullets -->

							<p class="animated">
								<strong>Examples of food supplements prescribed in England include Pro D3, Hux D3 and Valupak</strong>
							</p>

							<p class="animated">
								<strong>Food supplements may not meet the standards of assured quality required by medicines:</strong>
								
								Vitamin D-containing food supplements have shown large variation in their cholecalciferol content (between 8% and 201% of the labelled amount).<sup>4</sup> The cholecalciferol content of food supplements and compounded vitamin D preparations may be extremely variable (between 9% and 146% of stated dose).<sup>5</sup> Not only can there be variation between different food supplements, but there may also be variation among different pills from the same bottle.<sup>5</sup>
							</p>
						</div><!-- /.compare__content -->
					</div><!-- /.compare__inner -->
				</div><!-- /.compare -->

				<div class="references references--secondary">
					<h6 class="animated">References:</h6>
					<ol class="list-references animated">
						<li class="animated">Human Medicines Regulations 2012 (SI 2012/1916).</li>
						
						<li class="animated">MHRA Guidance Note No. 8. A Guide To What Is A Medicinal Product. Revised November 2012 </li>
						
						<li class="animated">Department of Health. Food supplements Guidance notes on legislation implementing Directive 2002/46/EC on food supplements. Updated January 2012.</li>
						
						<li class="animated">
							Garg S et al. Evaluation of vitamin D medicines and dietary supplements and the physicochemical analysis of selected formulations.
							<br/>Journal of nutrition, health &amp; aging. 2013; 17(2): 158-161.
						</li>
						
						<li class="animated">LeBlanc ES et al. Over-the-Counter and Compounded Vitamin D: Is Potency What We Expect? JAMA Intern Med. 2013; 173(7): 585-586.</li>
					</ol><!-- /.list-references -->
				</div><!-- /.references -->
			</div><!-- /.shell -->
		</div><!-- /.main__content -->
	</div><!-- /.main -->

	<?php include 'includes/footer.php';?>
</div><!-- /.wrapper -->
</body>
</html>

