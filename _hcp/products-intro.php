<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Invita D3 - responsive project</title>

	<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />

	<!-- Vendor Styles -->

	<!-- App Styles -->
	<link rel="stylesheet" href="vendor/OwlCarousel2-develop/dist/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="css/style.css" />

	<!-- Vendor JS -->
	<script src="vendor/jquery-1.12.4.min.js"></script>
	<script src="vendor/OwlCarousel2-develop/dist/owl.carousel.min.js"></script>

	<!-- App JS -->
	<script src="js/functions.js"></script>
</head>

<body>
<div class="wrapper">
	<?php include 'includes/header.php';?>

	<div class="main">
		<div class="main__intro">
			<div class="shell">
				<?php include 'includes/nav.php';?>
	

			
				<h1 class="animated">Products</h1>

				<img class="products-intro animated" src="css/images/temp/products-intro.png" alt="">
			</div><!-- /.shell -->
		</div><!-- /.main__intro -->

		<div class="main__content">
			<div class="shell">
				<div class="categories">
					<div class="category animated">
						<h2>Oral solutions</h2>

						<div class="products-secondary">
							<div class="product-secondary product-secondary--darkpink">
								<h4>
									<a href="#" class="text-darkpink animated">InVita D3 50,000 IU/ml</a>
								</h4><!-- /.text-darkpink -->

								<a href="#" class="animated">
									<img src="css/images/temp/product-secondary4.png" alt="">
								</a>
							</div><!-- /.product-secondary -->

							<div class="product-secondary product-secondary--blue">
								<h4>
									<a href="#" class="text-blue animated">InVita D3 25,000 IU/ml</a>
								</h4><!-- /.text-darkpink -->

								<a href="#" class="animated">
									<img src="css/images/temp/product-secondary3.png" alt="">
								</a>
							</div><!-- /.product-secondary -->
						</div><!-- /.products-secondary -->
					</div><!-- /.category -->

					<div class="category animated">
						<h2>Soft gel capsules</h2>

						<div class="product-secondary product-secondary--pink">
							<h4>
								<a href="#" class="text-pink animated">InVita D3 800 IU</a>
							</h4><!-- /.text-pink -->

							<a href="#" class="animated">
								<img src="css/images/temp/product-secondary1.png" alt="">
							</a>
						</div><!-- /.product-secondary -->
					</div><!-- /.category -->

					<div class="category">
						<h2>Oral drops</h2>

						<div class="product-secondary product-secondary--green">
							<h4>
								<a href="#" class="text-green animated">InVita D3 2,400 IU</a>
							</h4><!-- /.text-green -->

							<a href="#" class="animated">
								<img src="css/images/temp/product-secondary2.png" alt="">
							</a>
						</div><!-- /.product-secondary -->
					</div><!-- /.category -->
				</div><!-- /.categories -->

				<div class="form-secondary animated">
					<form action="?" method="post">
						<div class="form__inner animated">
							<div class="form__head">
								<h1 class="text-orange animated">To hear about new and upcoming products enter your email opposite for email alerts.</h1><!-- /.text-orange -->
							</div><!-- /.form__head -->
							
							<div class="form__body animated">
								<div class="form__row">
									<label for="field-email" class="form__label">Email:</label>
									
									<div class="form__controls">
										<input type="text" class="form__field animated" name="field-email" id="field-email" value="" placeholder="">
									</div><!-- /.form__controls -->
								</div><!-- /.form__row -->

								<div class="form__actions">
									<input type="submit" value="Submit" class="form__btn animated">
								</div><!-- /.form__actions -->
							</div><!-- /.form__body -->
						</div><!-- /.form__inner -->
					</form>
				</div><!-- /.form -->
			</div><!-- /.shell -->
		</div><!-- /.main__content -->
	</div><!-- /.main -->

	<?php include 'includes/footer.php';?>
</div><!-- /.wrapper -->
</body>
</html>

