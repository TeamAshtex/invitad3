<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Invita D3 - responsive project</title>

	<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />

	<!-- Vendor Styles -->

	<!-- App Styles -->
	<link rel="stylesheet" href="vendor/OwlCarousel2-develop/dist/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="css/style.css" />

	<!-- Vendor JS -->
	<script src="vendor/jquery-1.12.4.min.js"></script>
	<script src="vendor/OwlCarousel2-develop/dist/owl.carousel.min.js"></script>

	<!-- App JS -->
	<script src="js/functions.js"></script>
</head>

<body>
<div class="wrapper">
	<?php include 'includes/header.php';?>

	<div class="main">
		<div class="main__intro">
			<div class="shell">
				<?php include 'includes/nav.php';?>
	


				<h1 class="animated">About InVita D3 - Dosing and Frequency</h1>

				<img class="dosing-and-frequency animated" src="css/images/temp/dosing-intro.png" alt="">

				<nav class="nav-tertiary animated">
					<ul>
						<li class="animated">
							<a href="#treatment">Treatment</a>
						</li>

						<li class="animated">
							<a href="#prevention">Prevention</a>
						</li>

						<li class="animated">
							<a href="#infants">Infants</a>
						</li>
					</ul>
				</nav><!-- /.nav-tertiary -->
			</div><!-- /.shell -->
		</div><!-- /.main__intro -->

		<div class="main__content main__content--secondary">
			<section class="section main__section section--gray-gradient animated" id="treatment">
				<div class="shell">
					<h2 class="text-orange animated">Treatment</h2>
				
					<p class="animated">NOS guidelines recommend a treatment loading dose of 300,000 IU for treatment of vitamin D deficiency.<sup>1</sup> Invita D3 50,000 IU is given as a weekly dose over 6 weeks for the treatment of vitamin D deficiency and is suitable for adults. Invita D3 25,000 IU is suitable for treatment in children and is taken once every 2 weeks for 6 weeks.<sup>1</sup></p>
				
					<div class="gadgets">
						<div class="gadget animated">
							<img src="css/images/temp/gadget1.png" alt="">
						</div><!-- /.gadget -->
				
						<div class="gadget animated">
							<img src="css/images/temp/gadget2.png" alt="">
						</div><!-- /.gadget -->
				
						<div class="gadget animated">
							<img src="css/images/temp/gadget3.png" alt="">
						</div><!-- /.gadget -->
					</div><!-- /.gadgets -->

					<a href="#" class="section__btn animated">
						<i class="ico-arrow-orange"></i>
					</a>
				</div><!-- /.shell -->
			</section><!-- /.section -->
			
			<section class="section main__section section--blue-pink-gradient animated" id="prevention">
				<div class="shell">
					<h2 class="text-blue animated">Prevention</h2>
				
					<p class="animated">
						<strong>Prevention of vitamin D3 is recommended in groups who may be at risk from vitamin D deficiency. These include: <sup>2-4</sup></strong>
					</p>
				
					<ul class="list-secondary section__list animated">
						<li class="animated">Older patients (particularly in the case of falls or fractures) or those who may be institutionalised or hospitalised</li>
						
						<li class="animated">Dark skinned individuals</li>
						
						<li class="animated">Those with limited effective sun exposure due to protective clothing or consistent use of sun screens</li>
						
						<li class="animated">Obese individuals</li>
						
						<li class="animated">Patients being evaluated for osteoporosis</li>
						
						<li class="animated">Use of certain medications (e.g. anticonvulsant medications, glucocorticoids)</li>
						
						<li class="animated">Patients with malabsorption, including inflammatory bowel disease and coeliac disease</li>
					</ul><!-- /.list-secondary -->
				
					<p class="animated">
						<strong>InvitaD3 25,000IU can be used for prevention of vitamin D deficiency from birth.</strong>
					</p>
				
					<div class="gadgets">
						<div class="gadget animated">
							<img src="css/images/temp/gadget4.png" alt="">
						</div><!-- /.gadget -->
				
						<div class="gadget animated">
							<img src="css/images/temp/gadget5.png" alt="">
						</div><!-- /.gadget -->
				
						<div class="gadget animated">
							<img src="css/images/temp/gadget6.png" alt="">
						</div><!-- /.gadget -->
				
						<div class="gadget animated">
							<img src="css/images/temp/gadget7.png" alt="">
						</div><!-- /.gadget -->
					</div><!-- /.gadgets -->

					<a href="#" class="section__btn animated">
						<i class="ico-arrow-blue"></i>
					</a>
				</div><!-- /.shell -->
			</section><!-- /.section -->
			
			<section class="section main__section section--green-gradient animated" id="infants">
				<div class="shell">
					<h2 class="text-green animated">InVita D3 dose - infants and children<sup>5</sup></h2><!-- /.text-green -->
				
					<div class="gadgets">
						<div class="gadget animated">
							<img src="css/images/temp/gadget-8-9.png" alt="">
						</div><!-- /.gadget -->
					</div><!-- /.gadgets -->
				
					<h6 class="animated">Reference:</h6>
				
					<ol class="list-references animated">
						<li class="animated">National Osteoporosis Society. Vitamin D and Bone Health: A Practical Clinical Guideline for Patient Management April 2013;v1.</li>
						
						<li class="animated">Consilient Health Ltd. InVita D3 50,000 IU Oral Solution Summary of Product Characteristics.</li>
						
						<li class="animated">Consilient Health Ltd. InVita D3 25,000 IU Oral Solution Summary of Product Characteristics.</li>
						
						<li class="animated">Consilient Health Ltd. InVita D3 800IU soft gel capsules Summary of Product Characteristics.</li>
						
						<li class="animated">Consilient Health Ltd. InVita D3 2,400IU oral drops Summary of Product Characteristics.</li>
					</ol><!-- /.list-references -->
				
					<a href="#header" class="link-primary section__link animated">back to the top</a>
				</div><!-- /.shell -->
			</section><!-- /.section -->
		</div><!-- /.main__content -->
	</div><!-- /.main -->

	<?php include 'includes/footer.php';?>
</div><!-- /.wrapper -->
</body>
</html>

