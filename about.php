<html>
<?php 
    include 'base/head.php';
?>
    <body class="patient_section res__aboutUs">
        <div class="wrapper h_mid fullwidth">
            <div class="patient_container h_mid fullwidth">
                <div class="patient_header fl fullwidth">
                    <div class="fr mobile_navigation">
                        <a href="javascript:void(0);" class="mobile_icon fr"></a>
                    </div>
                    <div class="navigation fr">
                    <?php include 'includes/patient/nav1.php';?>
                    </div>
                    <?php include 'includes/patient/logo.php';?>
                </div>
            </div>
            <div class="patient_content hcp_container h_mid">
                <div class="fl bg_banner fullwidth">
                    <div class="patient_container h_mid nav_container">
                        <div class="fullwidth fl main_nav res_nav">
                            <?php include 'includes/patient/nav2.php';?>
                        </div>
                    </div>
                </div>
                <div class="patient_container tabs_pos fullwidth">
                    <h2 class="cntr_txt fl fullwidth main_heading tab1 tabs-section">
                        About InVita D3 - What is InVita D3?
                    </h2>
                    <h2 class="cntr_txt fl fullwidth main_heading tab2 tabs-section">
                        About InVita D3 - Dosage and how to take InVita D3?
                    </h2>
                    <h2 class="cntr_txt fl fullwidth main_heading tab3 tabs-section">
                        About InVita D3 - When you should  not take InVita D3?
                    </h2>
                    <h2 class="cntr_txt fl fullwidth main_heading tab4 tabs-section">
                        About InVita D3 - When to talk to your GP?
                    </h2>
                    <div class="tabs fl fullwidth">
                        <ul class="fl fullwidth main_tab">
                            <li>
                                <a href="javascript:void(0);" class="faq1 active" data-tab="tab1">
                                    <i>1</i>
                                    <span>What Is Invita D3</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="faq2" data-tab="tab2">
                                    <i>2</i>
                                    <span>Dosage & how to take InVita D3 </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="faq3" data-tab="tab3">
                                    <i>3</i>
                                    <span class="long_tab">When should you not take InVita D3</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="faq4" data-tab="tab4">
                                    <i>4</i>
                                    <span>When to talk to your GP</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="fl fullwidth inaTab">
                            <li class="for_tab for_tab1">
                                <img src="images/patient/tab1.jpg" alt="Talk To Your Doctor"/>
                            </li>
                            <li class="for_tab for_tab2">
                                <img src="images/patient/tab2.jpg" alt="Talk To Your Doctor"/>
                            </li>
                            <li class="for_tab for_tab3">
                                <img src="images/patient/tab3.jpg" alt="Talk To Your Doctor"/>
                            </li>
                            <li class="for_tab for_tab4">
                                <img src="images/patient/tab4.jpg" alt="Talk To Your Doctor"/>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--Starting Tab number 4's Detail content section "By default its display none"-->
            <div class="fl fullwidth tabs-section tab4">
                <div class="fl fullwidth patient_container h_mid">
                    <ul class="fl fullwidth tab4_lines">
                        <li>
                            <p>Are undergoing treatment with certain medicines used to treat heart disorders (e.g. cardiac glycosides, such as digoxin).</p>
                        </li>
                        <li>
                            <p>Have sarcoidosis (an immune system disorder which may cause increased levels of vitamin D in the body).</p>
                        </li>
                        <li>
                            <p>Are taking medicines containing vitamin D, or eating foods or milk enriched with vitamin D.</p>
                        </li>
                        <li>
                            <p>Are likely to be exposed to a lot of sunshine whilst using your medicine.</p>
                        </li>
                        <li>
                            <p>Take additional supplements containing calcium so that your doctor can monitor you to ensure your blood calcium levels remain within an acceptable range.</p>
                        </li>
                        <li>
                            <p>Have kidney damage or disease – as your doctor may want to measure the levels of calcium in your blood or urine.</p>
                        </li>
                    </ul>
                    <div class="fl fullwidth download_btn">
                        <a href="leaflet.php" class="fl btn_leaflet">Download Package Leaflet</a>
                    </div>
                </div>
                <div class="fl fullwidth tab1_bg">
                    <div class="fl fullwidth patient_container h_mid tab4_2">
                        <h2 class="fl fullwidth main_heading heading_blue">
                            Tell your doctor, pharmacist or nurse if you are using, have recently used or might use any other medicines:
                        </h2>
                        <ul class="fl fullwidth tab4_lines">
                            <li class="heading_li">
                                <p class="">This is specially important if you are taking:</p>
                            </li>
                            <li>
                                <p>Medicines that act on the heart or kidneys, such as cardiac glycosides (e.g. digoxin) or diuretics (e.g. bendroflumethazide).</p>
                            </li>
                            <li>
                                <p>Medicines containing vitamin D or food that are particularly rich in vitamin D such as some types of vitamin D-enriched milk.</p>
                            </li>
                            <li>
                                <p>Actinomycin (a medicine used to treat some forms of cancer) 
                                    and imidazole antifungals (e.g. clotrimazole and ketoconazole, 
                                    medicines used to treat fungal infections).</p>
                            </li>
                        </ul>
                        <ul class="fl fullwidth tab4_lines">
                            <li class="heading_li">
                                <p class="">This is specially important if you are taking:</p>
                            </li>
                            <li>
                                <p>Antiepileptic medicines (anticonvulsants), barbiturates.</p>
                            </li>
                            <li>
                                <p>Glucocorticoids (steroid hormones such as hydrocortisone or prednisolone).</p>
                            </li>
                            <li>
                                <p>Medicines that lower the level of cholesterol in the blood (such as cholestyramine, or colestipol).</p>
                            </li>
                            <li>
                                <p>Certain medicines for weight loss that reduce the amount of fat your body absorbs (e.g. orlistat).</p>
                            </li>
                            <li>
                                <p>Certain laxatives (such as liquid paraffin).</p>
                            </li>
                        </ul>
                        <div class="fl fullwidth download_btn">
                            <a href="leaflet.php" class="fr btn_leaflet">Download Package Leaflet</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--Ending Tab number 4's Detail content section "By default its display none"-->
            <!--Starting Tab number 3's Detail content section "By default its display none"-->

            <div class="fl fullwidth tabs-section tab3">
                <div class="fl fullwidth patient_container h_mid">
                    <ul class="fl fullwidth">
                        <li class="main_det_li">
                            <img src="images/patient/tab3_1.png" class="fl"/>
                            <div class="tab_row_des fr">
                                <p class="fl desHe">
                                    <span class="fl desHpink">50,000 IU/ml oral solution</span>
                                    <span class="fl desHgrey">and</span>
                                    <span class="fl desHblue">InVita D3 25,000</span>
                                </p>
                                <ul class="det_per_row fl fullwidth">
                                    <li>
                                        <p>Are allergic to vitamin D or any of the other ingredients (tocopherol acetate, polyglyceryl oleate (E475), olive oil, refined, sweet orange peel oil)</p>
                                    </li>
                                    <li>
                                        <p> Have high levels of calcium in the blood (hypercalcaemia)</p>
                                    </li>
                                    <li>
                                        <p> Have high levels of calcium in the urine (hypercalciuria)</p>
                                    </li>
                                    <li>
                                        <p>Have disturbed parathyroid hormone metabolism (pseudohypoparathyroidism)</p>
                                    </li>
                                    <li>
                                        <p> Have kidney stones (renal calculi)</p>
                                    </li>
                                    <li>
                                        <p>  Have high levels of vitamin D in the blood (hypervitaminosis D)</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="main_det_li">
                            <img src="images/patient/tab3_2.png" class="fl"/>
                            <div class="tab_row_des fr">
                                <p class="fl desHe desHgreen">
                                    InVita D3 2,400 IU/ml oral drops, solution
                                </p>
                                <ul class="det_per_row fl fullwidth">
                                    <li>
                                        <p> Are allergic to vitamin D or any of the other ingredients of this medicine. (Listed in  section 6 of the package leaflet)</p>
                                    </li>
                                    <li>
                                        <p>Have high levels of calcium in your blood (hypercalcaemia) or urine (hypercalciuria)</p>
                                    </li>
                                    <li>
                                        <p>  Have kidney stones (renal calculi)</p>
                                    </li>
                                    <li>
                                        <p> Have high levels of vitamin D in your blood (hypervitaminosis D)</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="main_det_li">
                            <img src="images/patient/tab3_3.png" class="fl"/>
                            <div class="tab_row_des fr">
                                <p class="fl desHe desHpinkL">
                                    InVita D3 800 IU soft gel capsules
                                </p>
                                <ul class="det_per_row fl fullwidth">
                                    <li>
                                        <p> If you are under 12 years old</p>
                                    </li>
                                    <li>
                                        <p> Are allergic to vitamin D or any of the other ingredients of this medicine. (Listed in section 6 of the package leaflet)</p>
                                    </li>
                                    <li>
                                        <p> Have high levels of calcium in your blood (hypercalcaemia) or urine (hypercalciuria)</p>
                                    </li>
                                    <li>
                                        <p> Have kidney stones (renal calculi)</p>
                                    </li>
                                    <li>
                                        <p> Have high levels of vitamin D in your blood (hypervitaminosis D)</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!--Ending Tab number 3's Detail content section "By default its display none"-->
            <!--Starting Tab number 2's Detail content section "By default its display none"-->

            <div class="fl fullwidth tabs-section tab2">
                <div class="fl fullwidth tab2_bg">
                    <div class="fl fullwidth patient_container h_mid">
                        <h2 class="fl fullwidth main_heading desHe desHblue">
                            Treatment
                        </h2>
                        <div class="tab2_det1 fl fullwidth">
                            <div class="tab2_tratment">
                                <img src="images/patient/treatment1.png"/>
                            </div>
                            <div class="tab2_tratment">
                                <img src="images/patient/treatment2.png"/>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:void(0);" class="h_mid dwn_icon"></a>
                </div>
                <div class="fl fullwidth tab2_bg2">
                    <div class="fl fullwidth patient_container h_mid">
                        <h2 class="fl fullwidth main_heading desHe desHblue">
                            Prevention
                        </h2>
                        <div class="tab2_det1 fl fullwidth">
                            <div class="tab2_tratment">
                                <img src="images/patient/Prevention1.png"/>
                            </div>
                            <div class="tab2_tratment">
                                <img src="images/patient/Prevention2.png"/>
                            </div>
                            <div class="tab2_tratment">
                                <img src="images/patient/Prevention3.png"/>
                            </div>
                            <div class="tab2_tratment">
                                <img src="images/patient/Prevention4.png"/>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:void(0);" class="h_mid dwn_icon"></a>
                </div>
                <div class="fl fullwidth tab2_bg">
                    <div class="fl fullwidth patient_container h_mid">
                        <h2 class="fl fullwidth main_heading desHe desHgreen">
                            InVita D3 dose – infants and children
                        </h2>
                        <div class="tab2_det1 fl fullwidth">
                            <div class="tab2_tratment">
                                <img src="images/patient/childern1.png"/>
                            </div>
                        </div>
                        <a href="javascript:void(0);" class="fl desHgrey btt_top">Back to the top</a>
                    </div>
                </div>
            </div>
            <!--Ending Tab number 2's Detail content section "By default its display none"-->
            <!--Starting Tab Number 3's Detail content section "By Default its display block"-->
            <div class="fl fullwidth tabs-section tab1">
                <div class="fl fullwidth patient_container h_mid">
                    <h2 class="fl fullwidth desHe desHblack mar_0">
                        What InVita D3 is prescribed for
                    </h2>
                    <ul class="fl fullwidth marT_50">
                        <li class="main_det_li">
                            <img src="images/patient/tab1_1.png" class="fl"/>
                            <div class="tab_row_des fr">
                                <p class="fl desHe desHpink">
                                    50,000 IU/ml oral solution
                                </p>
                                <ul class="fl fullwidth desP desHgrey mar_0">
                                    <li>
                                        <p>InvitaD3 50,000 IU/ml oral solution is prescribed to treat vitamin D deficiency</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="main_det_li">
                            <img src="images/patient/tab1_2.png" class="fl"/>
                            <div class="tab_row_des fr">
                                <p class="fl desHe desHblue">
                                    InVita D3 25,000
                                </p>
                                <ul class="fl fullwidth desP desHgrey mar_0">
                                    <li class="sub_bullet">
                                        <p>InvitaD3 25,000 IU/ml oral solution is prescribed:</p>
                                        <span class="fl fullwidth">To treat vitamin D deficiency</span> 
                                        <span class="sub_or fl fullwidth">OR</span>
                                        <span class="fl fullwidth">
                                            To prevent vitamin D deficiency where it is felt that there 
                                            is an increased risk of vitamin D deficiency or the body is 
                                            facing an increased demand for vitamin D
                                        </span> 
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="main_det_li">
                            <img src="images/patient/tab3_2.png" class="fl"/>
                            <div class="tab_row_des fr">
                                <p class="fl desHe desHgreen">
                                    InVita D3 2,400 IU/ml oral drops, solution
                                </p>
                                <ul class="fl fullwidth desP desHgrey mar_0">
                                    <li>
                                        <p>
                                            To prevent vitamin D deficiency when it is felt that there is a significant risk of deficiency 
                                            or the body is facing an increased demand for vitamin D
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="main_det_li">
                            <img src="images/patient/tab3_3.png" class="fl"/>
                            <div class="tab_row_des fr">
                                <p class="fl desHe desHpinkL">
                                    InVita D3 800 IU soft gel capsules 
                                </p>
                                <ul class="fl fullwidth desP desHgrey mar_0">
                                    <li>
                                        <p>
                                            To prevent vitamin D deficiency where it is felt that there is an increased risk 
                                            of vitamin D deficiency or the body is facing an increased demand for vitamin D
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!--Ending Tab number 1's Detail content section "By default its display none"-->
            
            <!--Footer-->
            <?php include 'includes/patient/footer1.php';?>
            <!--End Footer-->
            <!--Footer Bottom-->
            <?php include 'includes/patient/footer2.php';?>
            <!--End Footer Bottom-->
        </div>
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
                var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                    "http://invita.alch.me/images/patient/nav_icon_hover.png"
                    )
            //--><!]]>
        </script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                setTimeout(carousel, 10000); // Change image every 5 seconds
            }
        </script>
        <script>
            var wow = new WOW(
                    {
                        boxClass: 'wow', // animated element css class (default is wow)
                        animateClass: 'animated', // animation css class (default is animated)
                        offset: 0, // distance to the element when triggering the animation (default is 0)
                        mobile: true, // trigger animations on mobile devices (default is true)
                        live: true, // act on asynchronously loaded content (default is true)
                        callback: function (box) {
                            // the callback is fired every time an animation is started
                            // the argument that is passed in is the DOM node being animated
                        },
                        scrollContainer: null // optional scroll container selector, otherwise use window
                    }
            );
            wow.init();
        </script>
    </body>
</html>